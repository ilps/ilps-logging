import sys
import os
from contextlib import contextmanager

from fabric.api import env, task, cd, prefix, sudo, hide, run
from fabric.colors import green, red, blue, yellow
from fabric.operations import prompt, put
from fabric.contrib import files

# Add the project dir to the Python path so we can import stuff from it
sys.path.append(os.path.dirname(__file__))

#################
# Load settings #
#################
@task
def staging():
    """Load settings required for staging."""
    env.area = 'staging'
    load_config()
    print green('Loaded %s settings ' % env.proj_name)


@task
def production():
    """Load settings required for production."""
    env.area = 'production'
    load_config()
    print green('Loaded settings: %s ' % env.proj_name)


def load_config():
    try:
        from ilpslogging.settings import FABRIC as conf
        conf = conf[env.area]
    except (ImportError, KeyError):
        print red('Settings for %s could not be loaded' % env.area)
        exit()

    env.proj_name = conf.get('PROJECT_NAME')
    env.hosts = conf.get('HOSTS', [])
    env.web_host = conf.get('WEB_HOSTNAME')
    env.deploy_user = conf.get('DEPLOY_USER')
    env.ssh_config_dir = conf.get('SSH_CONFIG_DIR')
    env.repo_host = conf.get('REPO_HOST')
    env.repo_user = conf.get('REPO_USER')
    env.repo_name = conf.get('REPO_NAME')
    env.db_host = conf.get('DB_HOST')
    env.db_port = conf.get('DB_PORT')
    env.db_user = conf.get('DB_USER')
    env.db_pass = conf.get('DB_PASS')
    env.db_name = conf.get('DB_NAME')
    env.proj_dir = conf.get('PROJECT_DIR')
    env.venv_dir = os.path.join(env.proj_dir, conf.get('VIRTUALENV_DIR'))
    env.reqs_path = os.path.join(env.proj_dir, conf.get('REQUIREMENTS_PATH'))

####################
# Config templates #
####################
# Each of the templates below is uploaded at deploy time, only if the
# contents have changed, in which case the 'reload' command is also
# fired.

TEMPLATES = {
    'kibana': {
        'local_path': 'deploy/%(area)s_kibana.js',
        'remote_path': '%(proj_dir)s/project_repo/kibana/config.js'
    },
    'nginx': {
        'local_path': 'deploy/%(area)s_nginx.conf',
        'remote_path': '/etc/nginx/sites-enabled/%(web_host)s',
        'reload_command': 'service nginx reload',
        'owner': 'root'
    },
    'supervisor': {
       'local_path': 'deploy/%(area)s_supervisord.conf',
       'remote_path': '/etc/supervisord/confs/%(proj_name)s.conf',
       'reload_command': 'supervisorctl -c /etc/supervisord/supervisord.conf update',
       'owner': 'root',
    },
    'settings': {
        'local_path': 'deploy/%(area)s_settings.py',
        'remote_path': '%(proj_dir)s/project_repo/local_settings.py',
        'owner': '%(deploy_user)s',
        'mode': '600'
    }
}

######################################
# Context for virtualenv and project #
######################################
@contextmanager
def virtualenv():
    """Run commands within the project's virtualenv."""
    with cd(env.venv_dir):
        with prefix("source %s/bin/activate" % env.venv_dir):
            yield


@contextmanager
def project():
    """Run commands within the project's directory."""
    with virtualenv():
        with cd(env.proj_dir):
            yield


@contextmanager
def update_changed_requirements():
    """
    Checks for changes in the requirements file across an update,
    and gets new requirements if changes have occurred.
    """
    get_reqs = lambda: run("cat %s" % env.reqs_path)
    old_reqs = get_reqs() if env.reqs_path else ""
    yield
    if old_reqs:
        new_reqs = get_reqs()
        if old_reqs == new_reqs:
            # Unpinned requirements should always be checked.
            for req in new_reqs.split("\n"):
                if req.startswith("-e"):
                    if "@" not in req:
                        # Editable requirement without pinned commit.
                        break
                elif req.strip() and not req.startswith("#"):
                    if not set(">=<") & set(req):
                        # PyPI requirement without version.
                        break
            else:
                # All requirements are pinned.
                return
        with virtualenv():
            sudo('pip install -r %s' % env.reqs_path, user=env.deploy_user)


##############################
# Various utils and wrappers #
##############################
@task
def apt_install(packages):
    """Installs one or more system packages via apt."""
    print blue('Installing %s' % packages)
    return sudo("apt-get install -y -q %s" % packages)

def get_templates():
    """Return the templates with env vars injected."""
    injected = {}
    for name, data in TEMPLATES.items():
        injected[name] = dict([(k, v % env) for k, v in data.items()])
    return injected


def upload_template_and_reload(name, exec_reload=True):
    """
    Upload a given template only if it has changed, and if so, reload
    the associated service.
    """
    template = get_templates()[name]
    local_path = template['local_path']
    remote_path = template['remote_path']
    reload_command = template.get('reload_command')
    owner = template.get('owner')
    mode = template.get('mode')

    remote_data = ''

    # Check if the remote file exist, if so, grab it's contents
    if files.exists(remote_path):
        with hide('stdout'):
            remote_data = sudo('cat %s' % remote_path)

    # Get the contents of the local template and fill out the env fields
    with open(local_path, 'r') as f:
        local_data = f.read()
        local_data %= env

    clean = lambda s: s.replace('\n', '').replace('\r', '').strip()

    # If the local amd remote template are identical, we don't have
    # to update anything.
    if clean(remote_data) == clean(local_data):
        return

    # Upload the new template to the remote hoste
    files.upload_template(local_path, remote_path, env, use_sudo=True,
        backup=False)

    # Set ownership and permissions on the remote file if necessary
    if owner:
        sudo('chown %s:%s %s' % (owner, owner, remote_path))
    if mode:
        sudo('chmod %s %s' % (mode, remote_path))

    # Remote the related service if necessary
    if reload_command and exec_reload:
        sudo(reload_command)


@task
def manage(command):
    with project():
        sudo('python project_repo/manage.py %s' % command, user=env.deploy_user)


@task
def create_db_role(user, passwd, psql_user='postgres'):
    """Create a password protected PostgreSQL user."""
    sudo('psql -c "CREATE USER %s WITH NOCREATEDB NOCREATEUSER '\
         'ENCRYPTED PASSWORD E\'%s\'"' % (user, passwd), user=psql_user)


@task
def create_db(db_name, owner, psql_user='postgres'):
    """Create a PostgreSQL database owned by owner."""
    sudo('psql -c "CREATE DATABASE %s WITH OWNER %s"' % (db_name, owner),
         user=psql_user)


@task
def setup_supervisord():
    """Upload supervisord base config and init scripts."""
    # Create the folder for storing supervisord configs
    sudo('mkdir -p /etc/supervisord/confs/')

    # Copy the base config
    supervisor_conf = put('deploy/supervisord_base.conf',
                          '/etc/supervisord/supervisord.conf', use_sudo=True)
    if supervisor_conf.failed:
        red('Copying deploy/supervisord_base.conf to remote host failed')
        return

    # Copy the supervisord init script
    supervisor_init = put('deploy/supervisord_init', '/etc/init.d/supervisord',
                          use_sudo=True, mode=766)
    if supervisor_init.failed:
        red('Copying deploy/supervisord_init to remote host failed')
        return

    sudo('update-rc.d supervisord defaults')
    sudo('service supervisord start')


###############################
# Setup, configure and deploy #
###############################
@task
def setup_server():
    """Install required system packages"""

    print blue('Updating system')
    sudo('apt-get update -y -q && apt-get upgrade -y -q')

    apt_install('python-software-properties')

    print blue('Adding nginx PPA')
    sudo('add-apt-repository -y ppa:nginx/stable')

    sudo('apt-get update -y')

    print blue('Installing utils')
    apt_install('build-essential cmake pkg-config git ntp vim htop wget')

    print blue('Installing Python tools')
    apt_install('python-dev python-setuptools')

    print blue('Installing postfix, nginx, PostgreSQL server and OpenJDK')
    apt_install('postfix nginx postgresql postgresql-contrib '
                'postgresql-server-dev-9.1 openjdk-7-jre-headless')

    print blue('Installing PIP')
    sudo('easy_install pip')

    print blue('Installing virtualenv')
    sudo('pip install virtualenv')

    print blue('Installing supervisor')
    sudo('pip install supervisor --pre')

    print blue('Installing ElasticSearch')
    with cd('/root'):
        sudo('wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-0.90.5.deb')
        sudo('dpkg -i elasticsearch-0.90.5.deb')
        sudo('rm -rf elasticsearch-0.90.5.deb')

@task
def setup_project():
    """Setup the initial project environment.

    Firt create a private/public keypair to access the project's Git
    repo and add the key to the SSH config. Create the project
    directory, clone the repo and install all required packages found
    in requirements.txt through pip.
    """

    # Generate a keypair for deploying
    key_path = os.path.join(env.ssh_config_dir, env.proj_name)
    if not files.exists(key_path, use_sudo=True):
        print blue('Creating deploy key')
        sudo('mkdir -p %s' % env.ssh_config_dir)
        sudo('chown %s:%s %s' % (env.deploy_user, env.deploy_user,
                                 env.ssh_config_dir))
        sudo('ssh-keygen -t rsa -C "%s" -f %s' % (env.proj_name, key_path),
             user=env.deploy_user)
        sudo('cat %s.pub' % key_path)
        print yellow('Use the public key above as a deploy key for the '
                     'project\'s Git repository')
        prompt('Did you add the deploy-key to the Git repository?',
               default='yes')

        # Append project specific SSH params to ~/.ssh/config. Git uses
        # this config when cloneing and pulling the project's repo.
        ssh_config = """Host %s\n"""\
                     """    HostName %s\n"""\
                     """    User %s\n"""\
                     """    IdentityFile ~/.ssh/%s\n\n"""\
                     % (env.proj_name, env.repo_host, env.repo_user,
                        env.proj_name)
        files.append(os.path.join(env.ssh_config_dir, 'config'), ssh_config,
                     use_sudo=True)

    # Create the project directory
    print blue('Creating project directories')
    sudo('mkdir -p %s' % env.proj_dir)
    sudo('chown -R %s:%s %s' % (env.deploy_user, env.deploy_user,
                                env.proj_dir))

    # Initialize the project's virtualenv
    print blue('Initializing project\'s virtualenv')
    sudo('virtualenv --distribute %s' % env.venv_dir, user=env.deploy_user)

    # Clone the project's Git repo and install required Python packages
    with project():
        print blue('Cloning project\'s Git repository')
        sudo('git clone %s:%s project_repo' % (env.proj_name, env.repo_name),
             user=env.deploy_user)

        print blue('Installing required Python packages')
        sudo('pip install -r %s' % env.reqs_path, user=env.deploy_user)

    # Update the project's template and restart services
    for name in get_templates():
        upload_template_and_reload(name, False)

    # Create db and db user
    create_db_role(env.db_user, env.db_pass)
    create_db(env.db_name, env.db_user)

    # Init the db
    manage('create_tables')


@task
def deploy():
    """ """
    # Update the project's template and restart services
    for name in get_templates():
        upload_template_and_reload(name)

    with project():
        # Write the SHA of the current version to previous.commit in
        # order to rollback to this version later.
        with cd('project_repo'):
            sudo('git rev-parse HEAD > ../previous.commit',
                 user=env.deploy_user)

            # Pull the latest version from the project's Git repo
            with update_changed_requirements():
                sudo('git pull origin master -f', user=env.deploy_user)

        # Update requirements
        sudo('pip install -r %s' % env.reqs_path, user=env.deploy_user)

        # Reload uwsgi
        sudo('kill -HUP `cat %s/uwsgi.pid`' % env.proj_dir)

        # Build docs
        with cd('project_repo/docs/'):
            sudo('make html', user=env.deploy_user)

    # Build the JS SDK
    manage('build_jssdk')
