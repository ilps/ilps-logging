from werkzeug.wsgi import DispatcherMiddleware

from ilpslogging import frontend, api

application = DispatcherMiddleware(frontend.create_app(), {
    '/api/v0': api.create_app()
})
