RESTful API
===========

ILPSLogging provides a RESTful API to store new events and to query already logged events.

The API uses simple patterns and built-in HTTP features to receive commands and return responses. This allows for easy communication with the API from a wide variety of environments, from command-line utilities and libraries to direct communication from within a browser.

All API requests require authentication by using the HTTP Basic Authentication scheme in combination with a valid API key. Each ILPSLogging project has one or more API keys associated with it. An API key can have one of two roles: it can be used to query stored events (read) or to store new events (write). These roles are assigned when creating a new API key.

The API expects JSON content in requests, and returns JSON content in all of it responses (including errors). All data in requests are expected to be encoded in UTF-8. All data in responses will be encoded in UTF-8.

Logging events
--------------

.. http:post:: /api/v0/log

   Log a new event to a project. The API key used for authentication determines to which project the event is logged. The event data is POSTed as a JSON *object*. For more information on how events should be structured, see :doc:`datamodel`.

   **Example request**:

   .. sourcecode:: http

      $ curl -u <api_key>: http://logging.ilps.science.uva.nl/api/v0/log -d '{
        "created_at": "2013-07-26T14:32:44.590+02:00",
        "state": {
            "query": {
                "query_string": "my super interesting query"
            }
        },
        "event_type": "query_results",
        "event_properties": {
            "results": ["doc1", "doc23", "doc1848"],
            "n_total_results": 3,
            "n_displayed_results": 3,
            "query_time_ms": 800
        }
      }'

   **Example response**:

   .. sourcecode:: http

     HTTP/1.0 200 OK
     Content-Type: application/json
     Content-Length: 54
     Access-Control-Allow-Origin: *
     Access-Control-Allow-Methods: POST
     Access-Control-Max-Age: 21600
     Access-Control-Allow-Headers: Authorization

     {
         "id": "bzHcuiSwQUqRoZVz-otiCQ",
         "status": "ok"
     }

   :statuscode 200: **Success.**
   :statuscode 400: **Invalid request.** This occurs when something is missing or malformed in the posted data. The response body JSON contains additional information about why the request failed.
   :statuscode 401: **No authorization.** There was no valid API key provided with the request.
   :statuscode 403: **Forbidden.** The API key does not allow logging of new events.
   :statuscode 500: **Server error.** A server side problem has occurred. The response body JSON contains additional information about why the request failed.


.. http:post:: /api/v0/bulklog

   Log multiple new events to a project in a single API call. The API key used for authentication determines to which project the event is logged. The event data is POSTed as a JSON *array*, where each item in the array is an event *object*. For more information on how events should be structured, see :doc:`datamodel`.

   **Example request**:

   .. sourcecode:: http

      $ curl -u <api_key>: http://logging.ilps.science.uva.nl/api/v0/bulklog -d '[
        {
          "created_at": "2013-07-26T14:32:43.790+02:00",
          "state": {
              "query": {
                  "query_string": "my super interesting query"
              }
          },
          "event_type": "query",
          "event_properties": null
        },
        {
          "created_at": "2013-07-26T14:32:44.590+02:00",
          "state": {
              "query": {
                  "query_string": "my super interesting query"
              }
          },
          "event_type": "query_results",
          "event_properties": {
              "results": ["doc1", "doc23", "doc1848"],
              "n_total_results": 3,
              "n_displayed_results": 3,
              "query_time_ms": 800
          }
        }
      ]'

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json
      Content-Length: 20
      Access-Control-Allow-Origin: *
      Access-Control-Allow-Methods: POST
      Access-Control-Max-Age: 21600
      Access-Control-Allow-Headers: Authorization

      {
          "status": "ok"
      }

   :statuscode 200: **Success.**
   :statuscode 400: **Invalid request.** This occurs when something is missing or malformed in the posted data. The response body JSON contains additional information about why the request failed.
   :statuscode 401: **No authorization.** There was no valid API key provided with the request.
   :statuscode 403: **Forbidden.** The API key does not allow logging of new events.
   :statuscode 500: **Server error.** A server side problem has occurred. The response body JSON contains addition information about why the request failed.


Retrieving logged events
------------------------

.. http:post:: /api/v0/query

   Query the stored log events of project. The API key used for authentication determines which project is queried. Queries should be POSTed as a JSON *object* and formatted according to the `ElasticSearch Query DSL <http://www.elasticsearch.org/guide/reference/query-dsl/>`_.

   For more information on how stored events are structured, see :doc:`datamodel`.

   **Example request**:

   .. sourcecode:: http

      $ curl -u <api_key>: http://logging.ilps.science.uva.nl/api/v0/query -d '{
        "query": {
          "match_all": {}
        }
      }'

   **Example response**:

   .. sourcecode:: http

      HTTP/1.0 200 OK
      Content-Type: application/json
      Content-Length: 181
      Date: Sun, 29 Sep 2013 16:23:30 GMT

      {
        "_shards": {
          "failed": 0,
          "successful": 5,
          "total": 5
        },
        "hits": {
          "hits": [],
          "max_score": null,
          "total": 0
        },
        "timed_out": false,
        "took": 1
      }

   :statuscode 200: **Success.**
   :statuscode 400: **Invalid request.** This occurs when something is missing or malformed in the posted data. The response body JSON contains additional information about why the request failed.
   :statuscode 401: **No authorization.** There was no valid API key provided with the request.
   :statuscode 403: **Forbidden.** The API key does not allow querying for events.
   :statuscode 500: **Server error.** A server side problem has occurred. The response body JSON contains addition information about why the request failed.
