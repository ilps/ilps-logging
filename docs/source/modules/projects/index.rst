:mod:`projects` -- Projects
===========================

.. automodule:: ilpslogging.projects

.. autoclass:: ProjectsService
    :members: