Data-model
==========

Within ILPSLogging, each (user) interaction is captured as an **event**. To allow for flexibility (in terms of event structure) and easy querying, events are stored as JSON documents inside `ElasticSearch <http://elasticsearch.org/>`_.

On the root level, each event consist of an *object* (dictionary or hash table) that contains the following fields:

* ``created_at``: the date and time on which the event was generated (formatted according to `ISO 8601 <http://en.wikipedia.org/wiki/ISO_8601>`_, including the user's timezone).
* ``received_at``: the date and time on which the event was received by the ILPSLogging service (formatted according to `ISO 8601 <http://en.wikipedia.org/wiki/ISO_8601>`_, including the ILPSLogging server timezone). Storing the time an event was received may come in handy in situations when it is not possible to immediately send events to the server, for example when working with mobile applications.
* ``state``: contains information about the current 'state' of the user and/or application. For example: the ID of the user that generated the event, the screen resolution of the user, the variant of the interface that is served to the user (in case of A/B testing).
* ``event_type``: the type of interaction this event describes, for example: a mouse click, pagination action or query.
* ``event_properties``: additional data describing the event. In case of a mouse click, ``event_properties`` should contain a dictionary with the coordinates of the mouse and the name of the UI element that was clicked. The exact data that the ``event_properties`` element should contain differs per ``event_type``.

The five fields mentioned above are required for each event. For a collection of general events (clicks, mouse movements, queries, etc.), predefined structures of :ref:`events` are provided.

Using these predefined events and their corresponding data structures allows for comparative evaluation across systems and simplifies development of analysis tools. However, this does not mean that ILPSLogging cannot be used to log events for which no predefined structure is available; as a developer you are free to define your own custom event types, event properties and states. **TODO: Refer to best practices!**

State
-----

The ``state`` object contains (almost) static parameters that describe the current status or context of the application, the user and his or her environment. The names of fields mentioned below are used by ILPSLogging analysis tools. By default, none of the fields are mandatory. However, some events may require the presence of specific state fields. Developers are free to add custom fields to the ``state`` object.

State fields used by ILPS Logging:

* ``user_id`` (*int* |*string*): a string or integer used to identify the user.
* ``screen_resolution`` (*object*): the resolution of the user's screen in pixels.

  * ``width`` (*int*)
  * ``height`` (*int*)

* ``viewport_dimensions`` (*object*): the size of the the user's viewport in pixels. The viewport is the portion of the browser's canvas that is directly visible without scrolling. This is *not* the same as the user's screen resolution, as most users will not browse on their full screen.

  * ``width`` (*int*)
  * ``height`` (*int*)

* ``browser_dimensions`` (*object*): the size of the browser window in pixels. The browser window size covers the full size of the browser, including all interface elements such as the viewport, toolbars and the status bar.

  * ``width`` (*int*)
  * ``height`` (*int*)

* ``current_location`` (*str*): the address of the page the user is currently viewing.
* ``query`` (*object*): the query issued by the user of which he or she is currently viewing the results. The object consists of the query string (``query_string``) and may contain additional properties (``query_properties``) such as active filters and query settings.

When you create a new project, you have the option to enable IP and user-agent logging. When enabled, this information is automatically captured in the following fields:

* ``ip_address`` (*str*): the IP-address of the user.
* ``user_agent_string`` (*str*): the user-agent string of the user, for example ``Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4``.
* ``platform`` (*str*): the name of the user's platform (for example: ``linux``, ``windows`` or ``macos``).
* ``browser`` (*str*): the name of the user's browser (for example: ``chrome``, ``firefox`` or ``safari``).
* ``browser_version`` (*str*): the version number of the browser (for example: ``29.0.1547.76``).
* ``language`` (*str*): the language of the browser (for example: ``en-US``).

.. _events:

Events
------

.. _eventlogin:

``login``
^^^^^^^^^^^^^^^^^

Issued when the user logs in with the application.

**Fields**:

* ``state.user_id`` (*str* |*int*): a integer or string that uniquely identifies the user.
* ``event_properties`` (*str* |*object* |*int*): additional information about the user.

**Required fields**:

* ``state.user_id``

**Example**:

.. code-block:: json

    {
        "created_at": "2013-07-18T22:37:43.536864+02:00",
        "received_at": "2013-07-18T22:37:44.536864+02:00",
        "state": {
            "user_id": 123
        },
        "event_type": "login",
        "event_properties": null
    }


.. _eventlogout:

``logout``
^^^^^^^^^^^^^^^

Issued when the user logs out from the application.

**Example**:

.. code-block:: json

    {
        "created_at": "2013-07-18T22:37:43.536864+02:00",
        "received_at": "2013-07-18T22:37:44.536864+02:00",
        "state": {
            "user_id": 123
        },
        "event_type": "logout",
        "event_properties": null
    }

``mouse_movements``
^^^^^^^^^^^^^^^^^^

Captures the mouse movements of a user. A ``mouse_movements`` event is issued every *n* seconds (see :ref:`config`) and contains an array in which each item is an object with mouse position information. The object contains the exact time of the movement, the x/y coordinates of the mouse (relative to the document), and the current resolution of the viewport.

We recommend to record a maximum of one position change every 40 milliseconds (which is equivalent to a 25 FPS video). Automatic logging of mouse movements in this format can be enabled in the :ref:`config`.

**Fields**:

* ``event_properties.*.timestamp`` (*str*): the time the movement was recorded.
* ``event_properties.*.mouse_position.x`` (*int*): the x-coordinate of the mouse pointer.
* ``event_properties.*.mouse_position.y`` (*int*): the y-coordinate of the mouse pointer.
* ``event_properties.*.viewport_dimensions.width`` (*int*): the width of the browsers viewport at the time of the mouse movement.
* ``event_properties.*.viewport_dimensions.height`` (*int*): the height of the browsers viewport at the time of the mouse movement.


**Required fields**:

* ``event_properties.*.timestamp``
* ``event_properties.*.mouse_position.x``
* ``event_properties.*.mouse_position.y``
* ``event_properties.*.viewport_dimensions.width``
* ``event_properties.*.viewport_dimensions.height``

**Example**:

.. code-block:: json

    {
        "created_at": "2013-07-18T22:37:43.536864+02:00",
        "received_at": "2013-07-18T22:37:44.536864+02:00",
        "state": {
            "user_id": 123
        },
        "event_type": "mouse_movements",
        "event_properties": [
            {
                "timestamp": "2013-07-18T22:37:42.136864+02:00",
                "mouse_position": {
                    "x": 200
                    "y": 300
                },
                "viewport_dimensions": {
                    "width": 1280,
                    "height": 790
                }
            },
            {
                "timestamp": "2013-07-18T22:37:42.236864+02:00",
                "mouse_position": {
                    "x": 300
                    "y": 300
                },
                "viewport_dimensions": {
                    "width": 1280,
                    "height": 790
                }
            },
            {
                "timestamp": "2013-07-18T22:37:42.336864+02:00",
                "mouse_position": {
                    "x": 400
                    "y": 300
                },
                "viewport_dimensions": {
                    "width": 1280,
                    "height": 790
                }
            }
        ]
    }


``mouse_click``
^^^^^^^^^^^^^^^

Captures the mouse clicks of a user. For each click the position of the mouse pointer is registered, as well as information about the clicked DOM element.

Automatic logging of mouse clicks in this format can be enabled in the :ref:`config`.

**Fields**:

* ``event_properties.mouse_position.x`` (*int*): the x-coordinate of the mouse pointer.
* ``event_properties.mouse_position.y`` (*int*): the y-coordinate of the mouse pointer.
* ``event_properties.node_name`` (*str*): the name of the clicked element (e.g. "div", "a" or "img").
* ``event_properties.node_id`` (*str*): the ID of the clicked element.
* ``event_properties.node_classes`` (*array*): A list with the class names of the clicked element.
* ``event_properties.node_src`` (*str*): The "src" attribute value of the clicked element.
* ``event_properties.node_href`` (*str*): The "href" attribute value of the clicked element.
* ``event_properties.node_data_attrs`` (*object*): This object consist of the "data-ilpslogging-*" attribute key/value-pairs of the clicked element, and can be used as a convenience method for developers to log custom attributes automatically.

**Required fields**:

* ``event_properties.mouse_position.x``
* ``event_properties.mouse_position.y``
* ``event_properties.node_name``

**Example**:

.. code-block:: json

    {
        "received_at": "2013-07-25T14:40:22.238607+02:00",
        "created_at": "2013-07-25T14:39:26.970+02:00",
        "event_type": "mouse_click",
        "state": {
            "user_id": 123
        },
        "event_properties": {
            "mouse_position": {
                "y": 266,
                "x": 394
            },
            "node_name": "a",
            "node_id": "logo",
            "node_classes": ["large"],
            "node_href": "http://localhost/",
            "node_data_attrs": {
                "logovariant": "colored-extra-large"
            }
        }
    }

.. _eventquery:

``query``
^^^^^^^^^

Issued when the user submits a new query.

**Fields**:

* ``state.query.query_string`` (*str*): the query string as submitted by the user.
* ``state.query.query_properties`` (*object*): optional information related to the query (e.g. filters or strategy selection).

**Required fields**:

* ``state.query.query_string``

**Example**:

.. code-block:: json

    {
        "received_at": "2013-07-25T17:08:07.257632+02:00",
        "created_at": "2013-07-25T17:07:08.870+02:00",
        "event_type": "query",
        "state": {
            "query": {
                "query_string": "my super interesting query",
                "query_properties": {
                    "filters": {
                        "collections": ["newspapers"]
                    }
                }
            }
        },
        "event_properties": null
    }

.. _eventqueryresults:

``query_results``
^^^^^^^^^^^^^^^^^

Captures the results that are returned after the user submitted a new query or changed query.

**Fields:**:

* ``state.query.query_string`` (*str*): the query string that led to the result set described in this event.
* ``event_properties.results`` (*array*): each item in the array represents a document in the result set. The array items can consist of strings or integers (e.g. the document IDs), or objects. By using objects to describe a single hit, developers are able to add more information that might be relevant during analysis (such as the document score, creation date or source).
* ``event_properties.n_total_results`` (*int*): the total number of documents in the result set.
* ``event_properties.n_displayed_results`` (*int*): the number of results that are displayed to the user.
* ``event_properties.query_time_ms`` (*int*): the time it took (in milliseconds) to retrieve the results.

**Required fields**:

* ``state.query.query_string``
* ``event_properties.results``

**Example**:

.. code-block:: json

    {
        "received_at": "2013-07-26T14:32:45.841661+02:00",
        "created_at": "2013-07-26T14:32:44.590+02:00",
        "state": {
            "query": {
                "query_string": "my super interesting query"
            }
        },
        "event_type": "query_results",
        "event_properties": {
            "results": ["doc1", "doc23", "doc1848"],
            "n_total_results": 3,
            "n_displayed_results": 3,
            "query_time_ms": 800
        }
    }

.. _eventpaginate:

``paginate``
^^^^^^^^^^^^

Issued when the user initiates a navigation action when browsing through a paginated set of results.

**Fields**:

* ``state.query.query_string`` (*str*): the query string that led to the result set described in this event.
* ``state.query.current_page_n`` (*int*): the number of the page the user was on before initiating a pagination action.
* ``event_properties.to_page_n`` (*int*): the number of the pge the user is navigating to.

**Required fields**:

* ``state.query.query_string``
* ``state.query.current_page_n``
* ``event_properties.to_page_n``

**Example**:

.. code-block:: json

    {
        "received_at": "2013-07-26T14:32:45.841661+02:00",
        "created_at": "2013-07-26T14:32:44.590+02:00",
        "event_type": "paginate",
        "state": {
            "query": {
                "query_string": "my super interesting query",
                "current_page": 1
            }
        },
        "event_properties": {
            "from_page_n": 1,
            "to_page_n": 2
        }
    }

