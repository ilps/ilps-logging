Welcome to ILPSLogging's documentation!
=======================================

Overview
========

.. toctree::
    :hidden:

    installation
    tutorial
    datamodel
    jslib
    api
    modules/index

:doc:`installation`
  Instructions on how to install the ILPSLogging server.

:doc:`tutorial`
  Start here for a quick overview on how to use ILPSLogging in your project.

:doc:`datamodel`
  A description of the log events supported by ILPSLogging.

:doc:`jslib`
  Documentation on how to use and expand the JavaScript library.

:doc:`api`
  Documentation of the REST API used to store and query event logs.

:doc:`modules/index`
  ILPSLogging code documentation organized by module.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

