import json
import requests
from sys import exc_info
from urlparse import urljoin
from ..core import IlpsLoggingError
from ..settings import ELASTICSEARCH_URL
from pyelasticsearch import (ElasticSearch, ElasticHttpNotFoundError,
                             ElasticHttpError, IndexAlreadyExistsError,
                             ConnectionError, Timeout)

class ElasticSearchService(object):
    """
    ElasticSearch service that provides an abstraction layer for creating and
    deleting indices, and makes sure that by default, no strings are indexed.
    """

    # TODO: this should probably support loading from a JSON file as well
    # TODO: support for getting and setting the default mapping
    default_mapping = {
        'mappings': {
            '_default_': {
                'dynamic_templates': [
                    {
                        'event_template': {
                            'match': '*',
                            'match_mapping_type': 'string',
                            'mapping': {
                                'type': 'string',
                                'index': 'not_analyzed'
                            }
                        }
                    }
                ],
                '_index': {
                    'enabled': True
                }
            }
        }
    }

    def __init__(self):
        self.ELASTICSEARCH_URL = ELASTICSEARCH_URL
        self.es = ElasticSearch(self.ELASTICSEARCH_URL)

    def search(self, query, index=None, doc_type=None, size=10, offset=0):
        """
        Execute the provided query.

        :param query: dict formatted in ES query DSL
        :param index: An (iterable of) index(es) to search. Omit to search all.
        :param doc_type: An (iterable of) doc_type(s) to search. Omit to search all.
        :param size: Limit number of results. Use with "offset" to paginate.
        :param offset: Offset from first result you want to fetch.
        """
        kwargs = {'size': size, 'es_from': offset, 'query': query}
        if index:
            kwargs['index'] = index
        if doc_type:
            kwargs['doc_type'] = doc_type

        try:
            resp = self.es.search(**kwargs)
        except ConnectionError:
            raise IlpsLoggingError('Could not connect to ElasticSearch')
        except Timeout:
            raise IlpsLoggingError('Request to ElasticSearch timed out')
        except ElasticHttpNotFoundError:
            raise IlpsLoggingError('ElasticSearch returned a 404. The index '
                                   '%s was not found.' % index)
        except ElasticHttpError as e:
            raise IlpsLoggingError('ElasticSearch returned HTTP status code '
                                   '%s with error message %s'
                                   % (e.status_code, e.error))
        except:
            raise IlpsLoggingError('An unexpected error occured: %s.' %
                                    exc_info()[0])

        return resp

    def index(self, index, doc_type, doc):
        """
        Index documents.
        """
        try:
            stored_doc = self.es.index(index, doc_type, doc)
        except ConnectionError:
            raise IlpsLoggingError('Could not connect to ElasticSearch')
        except Timeout:
            raise IlpsLoggingError('Request to ElasticSearch timed out')
        except ElasticHttpNotFoundError:
            raise IlpsLoggingError('ElasticSearch returned a 404. Change the'
                                   ' ELASTICSEARCH_URL config parameter')
        except ElasticHttpError as e:
            raise IlpsLoggingError('ElasticSearch returned HTTP status code '
                                   '%s with error message  %s'
                                   % (e.status_code, e.error))
        except:
            raise IlpsLoggingError('An unexpected error occured: %s.' %
                                    exc_info()[0])

        return stored_doc

    def bulk_index(self, docs):
        """
        Bulk index documents. "Docs" is an array, where each item in the array
        is expected to be a dictionary/hash table structured as follows:

        {
            'index': <name of the index>,
            'type': <document type>,
            'doc': <document to index>
        }

        :param docs: Array of documents to index.
        """
        if not docs:
            raise IlpsLoggingError('No documents were provided')

        bulk = ''
        for doc in docs:
            try:
                action = {'index': {'_index': doc['index'], '_type': doc['type']}}
            except KeyError as e:
                raise IlpsLoggingError('%s missing in document' % e.message)

            bulk += '%s\n%s\n' % (json.dumps(action), json.dumps(doc['doc']))

        r = requests.post(urljoin(self.ELASTICSEARCH_URL, '_bulk'), data=bulk)
        # Raise error if request fails, otherwise keep silent
        r.raise_for_status()

    def create_index(self, project_key=None):
        """
        Creates an index based on the project slug
        """
        if not project_key:
            raise IlpsLoggingError('No project key was provided')

        try:
            self.es.create_index(project_key, self.default_mapping)
        except ConnectionError:
            raise IlpsLoggingError('Could not connect to ElasticSearch')
        except Timeout:
            raise IlpsLoggingError('Request to ElasticSearch timed out')
        except ElasticHttpNotFoundError:
            raise IlpsLoggingError('ElasticSearch returned a 404. Change the'
                                   ' ELASTICSEARCH_URL config parameter')
        except IndexAlreadyExistsError:
            raise IlpsLoggingError('The index you are trying to create already'
                                   ' exists; this might indicate that the DB '
                                   'is inconsistent now.')
        except ElasticHttpError as e:
            raise IlpsLoggingError('ElasticSearch returned HTTP status code '
                                   '%s with error message %s'
                                   % (e.status_code, e.error))
        except:
            raise IlpsLoggingError('An unexpected error occured: %s.' %
                                    exc_info()[0])


    def delete_index(self, project_key):
        """
        Deletes an index given the project_key string
        """
        if not project_key:
            raise IlpsLoggingError('No project key was provided')

        try:
            self.es.delete_index(project_key)
        except ConnectionError:
            raise IlpsLoggingError('Could not connect to ElasticSearch')
        except Timeout:
            raise IlpsLoggingError('Request to ElasticSearch timed out')
        except ElasticHttpNotFoundError:
            raise IlpsLoggingError('ElasticSearch returned a 404. The index '
                                   '%s was not found.' % project_key)
        except ElasticHttpError as e:
            raise IlpsLoggingError('ElasticSearch returned HTTP status code '
                                   '%s with error message %s'
                                   % (e.status_code, e.error))
        except:
            raise IlpsLoggingError('An unexpected error occured: %s.' %
                                    exc_info()[0])

    def get_mapping(self, index, doc_type=None):
        """
        Get mapping for a specific index and type. Omit type to get mappings
        for all types in the index.

        :param index: the index to get the mapping for
        :param doc_type: a doctype (str), or array of doctypes
        """
        if not index:
            raise IlpsLoggingError('No index was provided')

        try:
            mapping = self.es.get_mapping(index=index, doc_type=doc_type)
        except ConnectionError:
            raise IlpsLoggingError('Could not connect to ElasticSearch')
        except Timeout:
            raise IlpsLoggingError('Request to ElasticSearch timed out')
        except ElasticHttpNotFoundError:
            raise IlpsLoggingError('ElasticSearch returned a 404. The index '
                                   '%s was not found.' % index)
        except ElasticHttpError as e:
            raise IlpsLoggingError('ElasticSearch returned HTTP status code '
                                   '%s with error message %s'
                                   % (e.status_code, e.error))
        except:
            raise IlpsLoggingError('An unexpected error occured: %s.' %
                                    exc_info()[0])

        return mapping
