from flask import Flask
from raven.contrib.flask import Sentry

from .core import db
from .helpers import register_blueprints

def create_app(package_name, package_path, settings_override=None):
    """ Returns a :class:`Flask` application instance configured with
    common functionality for the ILPSLogging.

    :param package_name: application package name
    :param package_path: application package path
    :param settings_override: a dictionary of settings to override
    """
    app = Flask(package_name, instance_relative_config=True)

    app.config.from_object('ilpslogging.settings')
    app.config.from_object(settings_override)

    if app.config['ENABLE_SENTRY']:
        sentry = Sentry(dsn=app.config['SENTRY_DSN'])
        sentry.init_app(app)

    db.init_app(app)

    register_blueprints(app, package_name, package_path)

    return app
