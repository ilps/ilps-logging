from hashlib import md5
from uuid import uuid4

from ..core import db


def hash_email(context):
    """ Hash email on update """
    return md5(context.current_parameters['email'].lower().strip()).hexdigest()


def random_token():
    """ Generates a UUID4 and returns the string value """
    return str(uuid4())


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    email_hash = db.Column(db.String(32), default=hash_email)
    name = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(120), nullable=False)
    email_verified = db.Column(db.Boolean, default=False, nullable=False)
    email_verification_token = db.Column(db.String(36), default=random_token,
                                         nullable=False)
    account_approved = db.Column(db.Boolean, default=False, nullable=False)
    account_approved_token = db.Column(db.String(36), default=random_token,
                                       nullable=False)
    active = db.Column(db.Boolean, default=False, nullable=False)
    admin = db.Column(db.Boolean(), default=False, nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)
    updated_at = db.Column(db.DateTime, default=db.func.now(),
                           onupdate=db.func.now(), nullable=False)

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def get_gravatar(self, size=40, default=u'retro'):
        return u'http://www.gravatar.com/avatar/%s?s=%s&d=%s' % (self.email_hash,
            size, default)

    def __repr__(self):
        return '<User: %r>' % self.email
