import re

from sqlalchemy.exc import IntegrityError
from flaskext.bcrypt import Bcrypt

from ..core import SqlService, IlpsLoggingError, db
from .models import User

R_EMAIL = re.compile(r'^.+@[^.].*\.[a-z]{2,10}$', re.IGNORECASE)
bcrypt = Bcrypt()


def load_user(user_id):
    """ Callback for reloading a user from the session. None is returned
    if the user does not exist."""
    user = User.query.get(int(user_id))
    if user and user.email_verified and user.account_approved and user.active:
        return user

    return None


def verify_user(email, password):
    """Check if the user's email/password combination is valid. The user
    object is returned if the combination is valid, `None` is returned
    in case of an invalid combination.

    :param email: the user's email address
    :param password: the user's password
    """
    user = User.query.filter_by(email=email).first()

    # The user doesn't exist
    if not user:
        return None

    # The password is incorrect
    if not bcrypt.check_password_hash(user.password, password):
        return None

    # The user did not yet verify his e-mail address
    if not user.email_verified:
        raise IlpsLoggingError('Please verify your e-mail address before '
                               'logging in.')

    if not user.account_approved:
        raise IlpsLoggingError('Your registration is not yet approved by '
                               'one of the ILPS Logging administartors.')

    if not user.active:
        raise IlpsLoggingError('Your account is suspended.')

    return user


class UsersService(SqlService):
    __model__ = User

    def create(self, **kwargs):
        """Returns a new, saved instance of the user model class.

        :param **kwargs: instance parameters
        """
        # Strip traling and leading whitespace for each string value in kwargs
        for field, value in kwargs.iteritems():
            if type(value) is str:
                kwargs[field] = value

        # Check for missing fields or fields that do not contain any value
        missing_fields = []
        required_fields = ['email', 'name', 'password']
        for field in required_fields:
            if field not in kwargs or not kwargs[field]:
                missing_fields.append(field)

        # Raise an exception when there are missing fields
        if missing_fields:
            raise IlpsLoggingError('Missing fields or required fields are '
                                   'empty', missing_fields)

        # Verify that a valid mailaddress is supplied
        if not R_EMAIL.match(kwargs['email']):
            raise IlpsLoggingError('Invalid e-mail address')

        # Hash the password
        kwargs['password'] = bcrypt.generate_password_hash(kwargs['password'], 12)

        try:
            user = self.save(self.new(**kwargs))
        except IntegrityError:
            raise IlpsLoggingError('A user with the same e-mail address already exists')

        return user

    def verify_email(self, token):
        """Mark the user's e-mail address as verified.

        :param token: the `email_verification_token` of the user
        """
        user = self.first(email_verification_token=token)

        if not user:
            raise IlpsLoggingError('Invalid e-mail verification token')

        if user.email_verified:
            raise IlpsLoggingError('E-mail address of this user is already '
                                   'verified')

        user.email_verified = True
        user = self.save(user)

        return user

    def approve_account(self, token):
        """Mark the user's account as approved.

        :param token: the `account_approved_token` of the user
        """
        user = self.first(account_approved_token=token)

        if not user:
            raise IlpsLoggingError('Invalid account approval token')

        if user.account_approved:
            raise IlpsLoggingError('This account is already approved')

        user.account_approved = True
        user.active = True
        user = self.save(user)

        return user

    def promote_admin(self, user_id=None, email=None):
        """Give an existing non-admin user admin rights.

        :param user_id: the `id` of the user
        :param email: the `email` of the user
        """
        if not user_id and not email:
            raise IlpsLoggingError('Provide at least a user id or email '
                                   'address')

        if user_id:
            user = self.first(id=user_id)
        elif email:
            user = self.first(email=email)
        else:
            user = None

        if not user:
            raise IlpsLoggingError('User does not exist')

        if user.admin:
            raise IlpsLoggingError('User already has admin rights')

        user.admin = True
        user = self.save(user)

        return user

    def revoke_admin(self, user_id=None, email=None):
        """Revoke a user's admin rights.

        :param user_id: the `id` of the user
        :param email: the `email` of the user
        """
        if not user_id and not email:
            raise IlpsLoggingError('Provide at least a user id or email '
                                   'address')

        if user_id:
            user = self.first(id=user_id)
        elif email:
            user = self.first(email=email)
        else:
            user = None

        if not user:
            raise IlpsLoggingError('User does not exist')

        if not user.admin:
            raise IlpsLoggingError('User is not an admin')

        user.admin = False
        user = self.save(user)

        return user

    def delete(self, model):
        """Immediately deletes the specified model instance.

        :param model: the model instance to delete
        """
        self._isinstance(model)
        db.session.delete(model)
        db.session.commit()
