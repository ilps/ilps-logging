DEBUG = True
SECRET_KEY = 'secret-keya'

SQLALCHEMY_DATABASE_URI = 'postgresql://ilpslogging:ilpslogging@127.0.0.1:5432/ilpslogging'

# The local time zone for this installation.
TIME_ZONE = 'Europe/Amsterdam'

# Sentry
ENABLE_SENTRY = True
SENTRY_DSN = 'http://e730f7006a324629b6481d9d207d2114:bb7a11f108b84c9d95f6beeda573507d@sentry.dispectu.com/10'

ELASTICSEARCH_URL = 'http://127.0.0.1:9200'

# Default sender of e-mails
DEFAULT_MAIL_SENDER = ('ILPS Logging', 'no-reply@ilps.science.uva.nl')

MESSAGES = {
    'email_verification_subject': 'Thanks for creating an ILPS Logging account',
    'email_verification_text': 'Dear %s,\n\nThank you for creating an ILPS '
                               'Logging account.\n\nTo verify your e-mail '
                               'address, please click the following link: %s\n\n'
                               'After verification, one of the ILPS Logging '
                               'administrators will grant you access to the '
                               'application. You will be notified by email as '
                               'soon as your account is approved.'
                               '\n\nRegards,\nThe ILPS Logging administrators',
    'request_approval_subject': '[ILPS Logging] New user registration',
    'request_approval_text': 'The following user registered a new ILPS Logging '
                             'user account:\n\nName: %s\nE-mail: %s\n\nClick '
                             'the following link to approve this registration '
                             'and grant the user access to the application: %s',
    'user_approved_subject': 'Your ILPS Logging account is approved',
    'user_approved_text': 'Dear %s,\n\nYour ILPS Logging account is approved '
                          'and activated.\n\nTo start using ILPS Logging, '
                          'visit: %s.\n\nRegards,\nThe ILPS Logging '
                          'administrators'
}

EDITABLE_PROJECT_PROPERTIES = ['description'] # , 'name']
MINIFY_WEBASSETS = False

# URL on which the Kibana application can be reached
KIBANA_URL = 'http://localhost:8000/#/dashboard'

# URL of the ILPSLogging JavaScript SDK
JSSDK_URL = '/static/jssdk/'
# Name of the JS file user should include in their websites
JSSDK_FILENAME = 'ilpslogging-0.2.min.js'

# URL where the HTML version of the Sphinx docs can be found
DOCS_URL = '/docs/'

# These settings are used for deployment by fabfile.py
FABRIC = {
    'staging': {
        # Unique project identifier
        'PROJECT_NAME': 'ilpslogging',
        # Hosts to deploy to
        'HOSTS': ['146.185.151.177'],
        # The (v)host on which the application is reached
        'WEB_HOSTNAME': 'ilpslogging.staging.dispectu.com',
        # Git repo info
        'REPO_HOST': 'git.dispectu.com',
        'REPO_USER': 'git',
        'REPO_NAME': 'dispectu/ilps-logging.git',
        # Directory on host(s) in which the project will be stored
        'PROJECT_DIR': '/var/www/ilpslogging',
        # Python virtual env dir relative to PROJECT_DIR
        'VIRTUALENV_DIR': 'virtualenv',
        # Location of PIP requirements file relative to PROJECT_DIR
        'REQUIREMENTS_PATH': 'project_repo/requirements.txt',
        # The user that runs the application
        'DEPLOY_USER': 'www-data',
        # Dir for storing SSH keys and config
        'SSH_CONFIG_DIR': '/var/www/.ssh',
        # Database settings
        'DB_HOST': '127.0.0.1',
        'DB_PORT': 5432,
        'DB_NAME': 'ilpslogging',
        'DB_USER': 'ilpslogging',
        'DB_PASS': 'b8ezebrAsp9t'
    }
}

# Allow any settings to be defined in local_settings.py which should be
# ignored in your version control system allowing for settings to be
# defined per machine.
try:
    from local_settings import *
except ImportError:
    pass
