from flask import current_app
from flask.ext.script import (Command, prompt, prompt_pass, prompt_bool,
                              Option, Manager)
from flask.ext.assets import Environment, Bundle
from webassets.filter.jinja2 import Jinja2

from ..core import db, IlpsLoggingError

from ..services import users


class AddUser(Command):
    """Create a new ILPS Logging user."""
    def run(self):
        # Make sure the account can be used immediately after creation
        user_opts = {
            'email_verified': True,
            'account_approved': True,
            'active': True
        }

        user_opts['email'] = prompt('Email address')
        user_opts['name'] = prompt('Name')
        user_opts['password'] = prompt_pass('Password')
        user_opts['admin'] = prompt_bool('Is admin', default=False)

        try:
            new_user = users.create(**user_opts)
        except IlpsLoggingError, error:
            print 'Failed to create user, reason: %s' % error.msg
            return

        print 'Created user: %s' % new_user


class PromoteAdmin(Command):
    """Give an existing user admin permissions"""

    option_list = (
        Option('--user', '-u', dest='user', help='The user\'s id or email '
               'address'),
    )

    def select_user(self):
        non_admin_users = users.__model__.query.filter(users.__model__.admin != True)

        choices = {}
        for user in non_admin_users:
            choices[user.id] = user

            print '[%s] %s (%s)' % (user.id, user.name, user.email)

        user = False
        while not user:
            user = prompt('Select the user you want give admin permissions')

            try:
                user = int(user)
            except:
                user = False

            if user and user not in choices.keys():
                user = False

            if not user:
                print 'Please select a user from the list'

        return user


    def run(self, user=None):
        # If no user is provided via a CLI option, provide the user with
        # a list of candidates to choose from
        if not user:
            user = self.select_user()

        email = None
        user_id = None

        try:
            user_id = int(user)
        except ValueError:
            email = user

        try:
            user = users.promote_admin(user_id=user_id, email=email)
        except IlpsLoggingError, error:
            print 'Failed to promote user to admin, reason: %s' % error.msg

        print 'Added admin permissions to user: %s (%s)' % (user.name,
                                                            user.email)


class RevokeAdmin(Command):
    """Revoke a user's admin permissions"""

    option_list = (
        Option('--user', '-u', dest='user', help='The user\'s id or email '
               'address'),
    )

    def select_user(self):
        admin_users = users.__model__.query.filter(users.__model__.admin == True)

        choices = {}
        for user in admin_users:
            choices[user.id] = user

            print '[%s] %s (%s)' % (user.id, user.name, user.email)

        user = False
        while not user:
            user = prompt('Select the user from which you want to revoke '
                          'admin rights')

            try:
                user = int(user)
            except:
                user = False

            if user and user not in choices.keys():
                user = False

            if not user:
                print 'Please select a user from the list'

        return user

    def run(self, user=None):
        # If no user is provided via a CLI option, provide the user with
        # a list of candidates to choose from
        if not user:
            user = self.select_user()

        email = None
        user_id = None

        try:
            user_id = int(user)
        except ValueError:
            email = user

        try:
            user = users.revoke_admin(user_id=user_id, email=email)
        except IlpsLoggingError, error:
            print 'Failed to revoke admin permissions, reason: %s' % error.msg

        print 'Revoked admin permissions of user: %s (%s)' % (user.name,
                                                              user.email)


users_commands = Manager(usage='Perform user operations')
users_commands.add_command('add', AddUser())
users_commands.add_command('admin_revoke', RevokeAdmin())
users_commands.add_command('admin_promote', PromoteAdmin())

class BuildJSSDK(Command):
    """Build the ILPS Logging JavaScript SDK """
    def run(self):
        with current_app.test_request_context():
            webassets = Environment()
            webassets.config['CLOSURE_COMPRESSOR_OPTIMIZATION'] = 'SIMPLE_OPTIMIZATIONS'
            jinja_filter = Jinja2(context={'js_sdk_url':
                                            current_app.config['JSSDK_URL']})
            jssdk = Bundle('jssdk/ilpslogging-0.2.js', filters=jinja_filter,
                           output='jssdk/%s' % current_app.config['JSSDK_FILENAME'])
            jssdk.build(webassets)


class CreateTables(Command):
    """ Create all db tables """
    def run(self):
        with current_app.test_request_context():
            db.create_all()
            print 'Created tables'


class DropTables(Command):
    """Drop all db tables"""
    def run(self):
        with current_app.test_request_context():
            db.drop_all()
            print 'Dropped tables'


db_commands = Manager(usage='Perform database operations')
db_commands.add_command('tables_create', CreateTables())
db_commands.add_command('tables_drop', DropTables())
