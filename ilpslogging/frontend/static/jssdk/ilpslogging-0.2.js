(function(window, undefined) {
    var ILPSLogging = {};
    var config = {};
    var state = {};
    var events_queue = [];
    var mouse_move = {};
    var event_listeners = {};

    var local_storage_key = 'ilpslogging';

    if (window.ILPSLogging) {
        return;
    }

    function loadScript(url, callback) {
        var script = document.createElement('script');

        script.type  = 'text/javascript';
        script.async = true;
        script.src   = url;

        var entry = document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(script, entry);

        if (script.addEventListener)
            script.addEventListener('load', callback, false);
        else {
            script.attachEvent('onreadystatechange', function() {
                if (/complete|loaded/.test(script.readyState))
                    callback();
            });
        }
    }

    /*
     * Execute the functions that are registered under event_name.
     *
     * @param {string} event_name The name that identifies the event
     * @param data optional event data
     */
    function trigger_event(event_name, data) {
        // Do nothing if no listeners are registerd
        if (!event_listeners[event_name]) {
            return;
        }

        if (config.debug) console.log('Triggering event ' + event_name);

        for (var i = 0; i < event_listeners[event_name].length; i++) {
            event_listeners[event_name][i](data);
        }
    }

    /*
     * Register a function that should be executed when a certain event is triggered.
     *
     * @param {string} event_name The name that identifies the event
     * @param {function} handler The function that should be executed when the event is triggered
     */
    ILPSLogging.subscribe = function(event_name, handler) {
        if (config.dubg) console.log('Registered listner on event ' + event_name);

        if (typeof event_listeners[event_name] === 'undefined') {
            event_listeners[event_name] = [];
        }

        event_listeners[event_name].push(handler);
    };

    /*
     * Deregister a function that is executed when a certain event is triggered.
     * @param {string} event_name The name that identifies the event
     * @param {function} handler The function to deregister
     */
    ILPSLogging.unsubscribe = function(event_name, handler) {
        if (!event_listeners[event_name]) {
            return;
        }

        if (config.debug) console.log('Unlistening on event ' + event_name);

        for (var i = 0; i < event_listeners[event_name].length; i++){
            if (event_listeners[event_name][i] === handler) {
                event_listeners[event_name].splice(i, 1);
                break;
            }
        }
    };


    ILPSLogging.init = function(user_config, callback) {
        if (!('api_url' in user_config)) {
            throw 'Missing option: api_url';
        }
        config.api_url = user_config.api_url;

        if (!('project_key' in user_config)) {
            throw 'Missing option: project_key';
        }
        config.project_key = user_config.project_key;

        if ('use_visitor_cookie' in user_config) {
            config.use_visitor_cookie = user_config.use_visitor_cookie;
        } else {
            config.use_visitor_cookie = true;
        }

        if ('log_current_location' in user_config) {
            config.log_current_location = user_config.log_current_location;
        } else {
            config.log_screen_resolution = true;
        }

        if ('log_viewport_dimensions' in user_config) {
            config.log_viewport_dimensions = user_config.log_viewport_dimensions
        } else {
             config.log_viewport_dimension = true;
        }

        if ('log_screen_resolution' in user_config) {
            config.log_screen_resolution = user_config.log_screen_resolution;
        } else {
            config.log_screen_resolution = true;
        }

        if ('log_browser_dimensions' in user_config) {
            config.log_browser_dimensions = user_config.log_browser_dimensions;
        } else {
            config.log_browser_dimensions = true;
        }

        if ('log_mouse_movements' in user_config) {
            config.log_mouse_movements = user_config.log_mouse_movements;
        } else {
            config.log_mouse_movements = false;
        }

        if ('maximal_mouse_sample_freq_ms' in user_config) {
            config.maximal_mouse_sample_freq_ms = user_config.maximal_mouse_sample_freq_ms;
        } else {
            config.maximal_mouse_sample_freq_ms = 60;
        }

        if ('mouse_movements_event_interval_ms' in user_config) {
            config.mouse_movements_event_interval_ms = 1000;
        }

        if ('log_mouse_clicks' in user_config) {
            config.log_mouse_clicks = user_config.log_mouse_clicks;
        } else {
            config.log_mouse_clicks = false;
        }

        if ('log_browser_close' in user_config) {
            config.log_browser_close = user_config.log_browser_close;
        } else {
            config.log_browser_close = false;
        }

        if ('post_events_queue_on_browser_close' in user_config) {
            config.post_events_queue_on_browser_close = user_config.post_events_queue_on_browser_close;
        } else {
            config.post_events_queue_on_browser_close = false;
        }

        if ('post_events_queue_interval_ms' in user_config) {
            config.post_events_queue_interval_ms = user_config.post_events_queue_interval_ms;
        } else {
            config.post_events_queue_interval_ms = 2000;
        }

        if ('debug' in user_config) {
            config.debug = user_config.debug;
        } else {
            config.debug = false;
        }

        // Get or set the visitor cookie, place the visitor key in the state
        if(config.use_visitor_cookie){
            var visitor_key = getVisitorCookie();
            if(visitor_key){
                state.visitor_key = visitor_key;
            }
            else {
                state.visitor_key = setVisitorCookie();
            }
        }

        // Load external libs
        loadScript('{{ js_sdk_url }}libs.js', function(){
            ILPSLogging.$ = ILPSLogging.jQuery = jQuery.noConflict(true);

            // Enable Cross-Origin Resource Sharing in IE <= 9
            ILPSLogging.jQuery.support.cors = true;

            // Start mouse movement logging
            if(config.log_mouse_movements){
                ILPSLogging.enableMouseMovementLogging();
            }

            // Start mouse click logging
            if(config.log_mouse_clicks){
                ILPSLogging.enableMouseClickLogging();
            }

            // Start the interval timer to periodically empty and send
            // the events_queue.
            setInterval(ILPSLogging.$.proxy(postEvents, this), config.post_events_queue_interval_ms);

            // Empty the events_queue before closing the browser window 
            if (config.post_events_queue_on_browser_close) {
                if (window.addEventListener) {
                    window.addEventListener('beforeunload', send_beforeunload);
                // IE =< 9 uses "attachEvent"
                } else if (window.attachEvent) {
                    window.attachEvent('beforeunload', send_beforeunload);
                }
            }

            if (typeof callback === 'function') {
                callback();
            }
            trigger_event('ready');

            if(!config.post_events_queue_on_browser_close) {
                postEvents();
            }
        });
    };

    function send_beforeunload(e) {
        if (config.log_browser_close) {
            logEvent('browser_close', null);
        }

        postEvents(false);
    }

    /*
     * Enable logging of mouse movements
     */
    ILPSLogging.enableMouseMovementLogging = function() {
        if (config.debug) console.log('enableMouseMovementsLogging');

        mouse_move = {
            moves_buffer: [],
            position_x: 0,
            position_y: 0,
            last_sampled: new Date().getTime()
        };

        ILPSLogging.$(document).bind('mousemove', ILPSLogging.$.proxy(handleMouseMove));

        // Send and empty the mouse movement buffer every n miliseconds
        mouse_move.empty_moves_buffer_interval = setInterval(ILPSLogging.$.proxy(emptyMouseMovesBuffer),
                                                             config.mouse_movements_event_interval_ms);
    };

    /*
     * Disable logging of mouse movements
     */
    ILPSLogging.disableMouseMovementLogging = function(){
        if (config.debug) console.log('disableMouseMovementsLogging');

        // Unbind the mouse move event
        ILPSLogging.$(document).unbind('mousemove', handleMouseMove);

        // Cancel the emptying of the mouse buffer
        clearInterval(mouse_move.empty_moves_buffer_interval);
    };

    /*
     * Handles generated events when the mouse is moved. Called on every
     * mouse move.
     * 
     * @param {object} e The mouse move event
     */
    function handleMouseMove(e){
        if (config.debug) console.log('handleMouseMove');

        var current_time = new Date().getTime();

        // Append the mouse position to the buffer only if it is larger or
        // equal to maximal_mouse_sample_freq_ms.
        if((current_time - mouse_move.last_sampled) >= config.maximal_mouse_sample_freq_ms){
            // Only append to the buffer if the mouse position changed
            if(mouse_move.position_x != e.pageX || mouse_move.position_y != e.pageY){
                var move_props = {
                    timestamp: getCurrentDatetime(),
                    mouse_position: {
                        x: e.pageX,
                        y: e.pageY
                    },
                    viewport_dimensions: getViewportDimensions()
                };

                mouse_move.moves_buffer.push(move_props);
                trigger_event('mouse_move', move_props);

                // Update the current mouse position
                mouse_move.position_x = e.pageX;
                mouse_move.position_y = e.pageY;
                mouse_move.last_sampled = current_time;
            }
        }
    }

    /*
     * Create and submit an event based on the mouse movements that are
     * currently in the moves buffer. Empties the buffer after creating
     * the event.
     */
    function emptyMouseMovesBuffer(){
        if (config.debug) console.log('emptyMouseMovesBuffer');

        if(mouse_move.moves_buffer.length > 0){
            // Make a copy of the buffer array containing the moves
            // we are about to create an event of and empty the buffer.
            var moves_buffer = mouse_move.moves_buffer.slice();
            mouse_move.moves_buffer.length = 0;

            logEvent('mouse_movements', moves_buffer);
        }
    }

    /*
     * Enable logging of mouse clicks.
     */
    ILPSLogging.enableMouseClickLogging = function(){
        if (config.debug) console.log('enableMouseClickLogging');

        ILPSLogging.$(document).bind('click', ILPSLogging.$.proxy(handleMouseClick));
    };

    /*
     * Disable logging of mouse clicks.
     */
    ILPSLogging.disableMouseClickLogging = function(){
        if (config.debug) console.log('disableMouseClickLogging');

        ILPSLogging.$(document).unbind('click', handleMouseClick);
    };

    /*
     * Handles generated mouse click events.
     *
     * @param {object} e The mouse click event
     */
    function handleMouseClick(e){
        if (config.debug) console.log('handleMouseClick');

        var click_properties = {
            mouse_position: {
                x: e.pageX,
                y: e.pageY
            },
            node_name: e.target.nodeName
        };

        if(e.target.id){
            click_properties.node_id = e.target.id;
        }

        if(e.target.className){
            // Check if we are dealing with a click on an SVG related element,
            // if so, use the classnames in 'baseVal'.
            if (e.target.className.constructor.name === 'SVGAnimatedString') {
                click_properties.node_classes = e.target.className.baseVal.split(' ');
            } else {
                click_properties.node_classes = e.target.className.split(' ');
            }
        }

        if(e.target.src){
            click_properties.node_src = e.target.src;
        }

        if(e.target.href){
            click_properties.node_href = e.target.href;
        }

        click_properties.node_data_attrs = {};
        var node_data_attrs = ILPSLogging.$(e.target).data();

        // Determine which data attrs should be logged
        if(!(ILPSLogging.$.isEmptyObject(node_data_attrs))){
            ILPSLogging.$.each(node_data_attrs, function (data_attr, data_val) {
                // Check if the attribute starts with the 'ilpslogging' prefix
                if (data_attr.slice(0, 11) == 'ilpslogging') {
                    // Remove 'ilpslogging' prefix and lower case the remaning
                    // attribute name (jQuery transforms data attr values to 
                    // camel case...)
                    data_attr = data_attr.slice(11, data_attr.length).toLowerCase();
                    click_properties.node_data_attrs[data_attr] = data_val;
                }
            });
        }

        logEvent('mouse_click', click_properties);
        trigger_event('mouse_click', click_properties);
    }

    /*
     * Record a new query. The 'query_string' and the optional
     * 'query_properties' object are added to the 'query' oject which is
     * added to 'state'. If 'state.query.current_page' is set, it will
     * be 'reset' to 1.
     *
     * @param {string} query_string The query string as submitted by the user
     * @param {object} query_properties Additional information about the query (optional)
     */
    ILPSLogging.query = function(query_string, query_properties){
        if (config.debug) console.log('query');

        if(!('query' in state)){
            state.query = {};
        }

        state.query.query_string = query_string;

        if(query_properties){
            state.query.query_properties = query_properties;
        }

        if(state.query.current_page){
            state.query.current_page = 1;
        }

        logEvent('query', null);
    };

    /*
     * Log the results that are returned by the last submitted query.
     *
     * @param {array} results_list A list of documents returned by the submitted query
     * @param {int} n_total_results The total number of hits returned by the query (optional)
     * @param {int} n_displayed_results The number of results that are displayed to the user (optional)
     * @param {int} query_time_ms The time it took (in milliseconds) to retrieve the results (optional)
     */
    ILPSLogging.queryResults = function(results_list, n_total_results, n_displayed_results, query_time_ms){
        if (config.debug) console.log('queryResults');

        var query_results = {
            results: results_list
        };

        if(n_total_results){
            query_results.n_total_results = n_total_results;
        }

        if(n_displayed_results){
            query_results.n_displayed_results = n_displayed_results;
        }

        if(query_time_ms){
            query_results.query_time_ms = query_time_ms;
        }

        logEvent('query_results', query_results);
    };

    /*
     * Log a paginate action. The number of the page the user is navigating
     * to ('to_page_n') will be written to 'state.query.current_page'.
     *
     * @param {int} to_page_n The number of the page the user is navigating to
     * @param {int} from_page_n The number of the page the user was on before initiating a paginate action (optional)
     */
    ILPSLogging.paginate = function(to_page_n, from_page_n){
        if (config.debug) console.log('paginate');

        var paginate_event = {
            to_page_n: to_page_n
        };

        if(from_page_n){
            paginate_event.from_page_n = from_page_n;
        }
        else if(state.query.current_page){
            paginate_event.from_page_n = state.query.current_page;
        }
        else {
            paginate_event.from_page_n = 1;
        }
        
        state.query.current_page = from_page_n;
        logEvent('paginate', paginate_event);
    };

    /*
     * Records a login event. The 'user_id' is added as an argument of
     * 'state', 'user_info' is an object that can be used to save
     * additional info about the user.
     *
     * @param {string} user_id The id that identifies the current user
     * @param {object} user_info An object represeting additonal user data (optional)
     * @param {boolean} log_event If set to false the state object is modified, but no login event will be generated
     */
    ILPSLogging.userLogin = function(user_id, user_info, log_event){
        if (config.debug) console.log('userLogin');
        state.user_id = user_id;

        if (log_event === false) {
            return;
        }

        if (typeof(user_info) !== "object") {
            logEvent('login', null);
        }
        else {
            logEvent('login', user_info);
        }
    };

    /*
     * Records a logout event. The 'user_id' will be removed from the
     * 'state' after logEvent is called.
     */
    ILPSLogging.userLogout = function(){
        logEvent('logout', null);
        delete state.user_id;
    };

    /*
     * Set a vistior_key to identify events from this visitor
     */
    function setVisitorCookie(){
        if (config.debug) console.log('setVisitorCookie');

        var visitor_key = generateVisitorKey();
        var expiry_date = new Date();
        expiry_date.setDate(expiry_date.getDate() + 3650);
        var cookie = visitor_key + '; expires=' + expiry_date.toUTCString();
        document.cookie = config.project_key + '=' + cookie;

        return visitor_key;
    }

    /*
     * Generate a UUID that can be used as value of the visitor cookie
     */
    function generateVisitorKey(){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c){
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    /*
     * Returns the visitor key if the cookie exists, else returns false
     */
    function getVisitorCookie(){
        if (config.debug) console.log('getVisitorCookie');

        var cookies = document.cookie.split(';');
        var cookie_key = config.project_key;

        for(var i = 0; i < cookies.length; i++){
            var cookie = cookies[i].split('=');

            if(cookie[0] === cookie_key){
                return cookie[1];
            }
        }

        return false;
    }

    /*
     * Get the current screen resolution
     *
     * @return {object}
     */
    function getScreenResolution(){
        if (config.debug) console.log('getScreenResolution');

        return {
            width: screen.width,
            height:     screen.height
        };
    }

    /*
     * Get the current dimensions of the viewport
     *
     * @return {object}
     */
    function getViewportDimensions() {
        if (config.debug) console.log('getViewportDimensions');

        return {
            width: ILPSLogging.$(window).width(),
            height: ILPSLogging.$(window).height()
        };
    }

    /*
     * Get the current dimensions of browser window
     *
     * @return {object}
     */
    function getBrowserDimensions(){
        if (config.debug) console.log('getBrowserDimensions');

        return {
            width: ILPSLogging.$(document).width(),
            height: ILPSLogging.$(document).height()
        };
    }

    /*
     * Get the current datetime as a string
     *
     * @return {string}
     */
    function getCurrentDatetime(){
        return ILPSLogging.moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    }

    /*
     * Publice function to get a copy of the state object
     *
     * @return {object}
     */
    ILPSLogging.getState = function() {
        return ILPSLogging.$.extend({}, state);
    };

    /*
     * Public function to set the state object
     *
     * @param {object} state 
     */
    ILPSLogging.setState = function(new_state) {
        state = new_state;
    };

    /*
     * Public function to log a custom event
     *
     * @param {string} event_type
     * @param {object} event_properties
     * @param {object} new_state  
     */
    ILPSLogging.logEvent = function(event_type, event_properties, new_state) {
        // Validate event_type
        if (typeof event_type !== 'string') {
            throw 'event_type must be a string';
        }
        else if (event_type.length === 0) {
            throw 'event_type cannot be an empty string';
        }

        // Validate event_properties
        if (typeof event_properties !== 'object' && event_properties !== null) {
            throw 'event_properties can only be an object or null';
        }

        // Set the state if the user provided an object
        if (typeof new_state === 'object') {
            state = new_state;
        }

        logEvent(event_type, event_properties);
    };

    /*
     * Generate a log event document and add it to the events queue
     *
     * @param {string} event_type The name of the event
     * @param {object} event_properties The object that describes the event
     */
    function logEvent(event_type, event_properties){
        if(config.log_current_location){
            state.current_location = window.location.href;
        }
        if(config.log_screen_resolution){
            state.screen_resolution = getScreenResolution();
        }
        if(config.log_viewport_dimensions){
            state.viewport_dimensions = getViewportDimensions();
        }
        if(config.log_browser_dimensions){
            state.browser_dimensions = getBrowserDimensions();
        }

        // Copy the contents from state into a new object to prevent
        // changes to state to be included in events that are already
        // submitted to the events_queue 
        var current_state = {};
        ILPSLogging.$.extend(current_state, state);

        var event_doc = {
            created_at: getCurrentDatetime(),
            state: current_state,
            event_type: event_type,
            event_properties: event_properties
        };

        addEventToQueue(event_doc);
    }

    function addEventToQueue(event_doc) {
        if (config.post_events_queue_on_browser_close) {
            events_queue.push(event_doc);
        } else {
            var queued_events = localStorage.getItem(local_storage_key);

            if (queued_events) {
                queued_events = JSON.parse(queued_events);
                queued_events.push(event_doc);
            } else {
                queued_events = [event_doc];
            }

            localStorage.setItem(local_storage_key, JSON.stringify(queued_events));
        }
    };

    function postEvents(async) {
        if (config.debug) console.log('postEvents');

        if (async === 'undefined') {
            async = true;
        }

        if (config.post_events_queue_on_browser_close && events_queue.length !== 0) {
            // Copy events and empty the buffer
            var events = events_queue.slice();
            events_queue.length = 0;

            trigger_event('submit_events_queue', queued_events);

            ILPSLogging.$.ajax({
                type: 'POST',
                url: config.api_url + '/api/v0/bulklog?api_key=' + config.project_key,
                contentType: 'text/plain; charset=UTF-8',

                data: JSON.stringify(events),
                async: async,
                timeout: 5000,
                success: function(data){
                    trigger_event('submit_events_response', data);
                },
                error: function(xhr, status, error) {
                    // console.log('xhr='+xhr+', status='+status+', error='+error);

                    // XXX: maybe we should instead check the
                    //      xhr.responseJSON.status field
                    if (xhr.status == 400) {
                        // don't requeue bad request response from server
                    }
                    else {
                        // Put the events we tried to post back on the events queue
                        events_queue.push.apply(events_queue, events);
                    }
                }
            });
        } else if (!config.post_events_queue_on_browser_close) {
            var queued_events = localStorage.getItem(local_storage_key);
            if (queued_events !== '[]') {
                localStorage.setItem(local_storage_key, '[]');

                trigger_event('submit_events_queue', queued_events);

                ILPSLogging.$.ajax({
                    type: 'POST',
                    url: config.api_url + '/api/v0/bulklog?api_key=' + config.project_key,
                    contentType: 'text/plain; charset=UTF-8',

                    data: queued_events,
                    async: async,
                    timeout: 5000,
                    success: function(data){
                        trigger_event('submit_events_response', data);
                    },
                    error: function(xhr, status, error) {
                        // console.log('xhr='+xhr+', status='+status+', error='+error);

                        // XXX: maybe we should instead check the
                        //      xhr.responseJSON.status field
                        if (xhr.status == 400) {
                            // don't requeue bad request response from server
                        }
                        else {
                            // Put the events we tried to post back on the events queue
                            events_queue.push.apply(events_queue, events);
                        }
                    }
                });
            }
        }
    }

    // Execute ready callbacks
    window.ILPSLogging = ILPSLogging;
    if (typeof window.ILPSLogging_ready === 'object') {
        for (var i=0; i < window.ILPSLogging_ready.length; i++) {
            if (typeof window.ILPSLogging_ready[i] === 'function') {
                window.ILPSLogging_ready[i]();
            }
        }
    }
})(this);