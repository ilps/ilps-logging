$(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $('.combined-graph-controls button').on('click', combinedGraphControls);
});

function combinedGraphControls(e){
    e.preventDefault();
    var button = $(e.currentTarget),
        list = $('.combined-graph-controls ul'),
        range = button.data('value');

    var currentlyActive = list.find('button.active');
    if (button[0] !== currentlyActive[0]){
        currentlyActive.removeClass('active');
        button.addClass('active');

        d3.json('/projects/' + project + '/graph?range=' + range, updateCombinedGraph);
    }
}

function sparklineGraph(){
    var margin = {top: 30, right: 10, bottom: 20, left: 10},
        width = 400,
        height = 150,
        xScale = d3.time.scale().nice(d3.time.hour, 1),
        yScale = d3.scale.linear(),
        line = d3.svg.line().x(X).y(Y).interpolate('basis');

    function chart(selection){
        selection.each(function(data){
            var minX = new Date(data.since),
                maxX = new Date(),
                maxY = d3.max(data.entries, function(d){ return d.count; });

            // Update scales
            xScale
                .domain([minX, maxX])
                .range([0, width - margin.left - margin.right]);

            yScale
                .domain([0, maxY])
                .range([height - margin.top - margin.bottom, 0]);

            var range = d3.time.hour.utc.range(d3.time.hour.offset(minX, -1), d3.time.hour.offset(maxX, 1), 1);
            data = range.map(function(d){
                var datapoint = _.find(data.entries, function(e){ return d.getTime() === e.time; });
                return datapoint ? [d, datapoint.count] : [d, 0];
            });


            // Select SVG element, if it exists
            var svg = d3.select(this).selectAll('svg').data([data]);

            // Otherwise, create chart skeleton
            var gEnter = svg.enter().append('svg').append('g').attr('class', 'inner');
            gEnter.append('path').attr('class', 'line');

            // // Update outer dimensions
            svg.attr('width', width)
               .attr('height', height);

            // // Update inner dimensions
            var g = svg.select('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            var path = g.select('.line')
                .attr('d', line);

            var pathLength = path.node().getTotalLength();

            path.attr('stroke-dasharray', pathLength + ' ' + pathLength)
                .attr('stroke-dashoffset', pathLength)
              .transition()
                .delay(250)
                .ease('quad-in-out')
                .duration(1000)
                .attr('stroke-dashoffset', 0);
        });
    }

    // The x-accessor for the path generator; xScale(xValue)
    function X(d) {
        return xScale(d[0]);
    }

    // The x-accessor for the path generator; yScale(yValue)
    function Y(d) {
        return yScale(d[1]);
    }

    chart.margin = function(val) {
        if (!arguments.length) return margin;
        margin = val;
        return chart;
    };

    chart.width = function(val) {
        if (!arguments.length) return width;
        width = val;
        return chart;
    };

    chart.height = function(val) {
        if (!arguments.length) return height;
        height = val;
        return chart;
    };

    return chart;
}

function combinedGraph(){
    var margin = {top: 10, right: 150, bottom: 30, left: 40},
        width = 760,
        height = 120,
        legendDistance = 25,
        color = d3.scale.category20(),
        xValue = function(d) { return d[0]; },
        yValue = function(d) { return d[1]; },
        xScale = d3.time.scale(),
        yScale = d3.scale.linear(),
        xAxis = d3.svg.axis().scale(xScale).orient('bottom').tickPadding(10).ticks(8),
        yAxis = d3.svg.axis().scale(yScale).orient('left').tickPadding(5).ticks(5),
        line = d3.svg.line().x(X).y(Y).interpolate('monotone'),
        intervalMapping = {
            'minute': {step: 1, interval: 'minute'},
            '5m': {step: 5, interval: 'minute'},
            '15m': {step: 5, interval: 'minute'},
            'hour': {step: 1, interval: 'hour'},
            'day': {step: 1, interval: 'day'},
            'week': {step: 1, interval: 'monday'}
        };

    function chart(selection){
        selection.each(function(data){
            color.domain(d3.keys(data.histograms));

            var events = color.domain().map(function(event_name){
                return {
                    name: event_name,
                    values: data.histograms[event_name].entries
                };
            });

            if(events.length === 0){
                $('.empty-message').html('<p class="no-events">There have been no events recorded in the selected interview. Please try another interval.</p>').show();
                $('.combined-graph').hide();
            } else {
                $('.combined-graph').show();
            }
            /*
                minX = new Date(d3.min(events, function(e){ return d3.min(e.values, function(v){ return v.time; }); }))
                maxX = new Date(d3.max(events, function(e){ return d3.max(e.values, function(v){ return v.time; }); }))
            */
            var minX = new Date(data.since),
                maxX = new Date(),
                maxY = d3.max(events, function(e){ return d3.max(e.values, function(v){ return v.count; }); });

            // Update scales and axes
            xScale
                .domain([minX, maxX])
                .range([0, width - margin.left - margin.right])
                .nice(d3.time[intervalMapping[data.interval].interval], intervalMapping[data.interval].step);
            yScale
                .domain([0, maxY])
                .range([height - margin.top - margin.bottom, 0]);
            xAxis
                .tickSize(-height + margin.top + margin.bottom, 0);
            yAxis
                .tickSize(-width + margin.left + margin.right, 0);

            // Note that we use UTC dates, not local dates: this is important when
            // generating ranges of dates, as midnight January 1st CEST is not on the
            // same date in UTC
            var range = d3.time[intervalMapping[data.interval].interval].utc.range(
                d3.time[intervalMapping[data.interval].interval].offset(minX, -1),
                d3.time[intervalMapping[data.interval].interval].offset(maxX, 1),
                intervalMapping[data.interval].step
            );

            // Map the found range to our data: i.e. add zeroes for timesteps in the range
            // that we don't have data for
            data = events.map(function(e){
                var values = range.map(function(d){
                    datapoint = _.findWhere(e.values, {time: d.getTime()});
                    if(datapoint){
                        return {time: d, count: datapoint.count};
                    } else {
                        return {time: d, count: 0};
                    }
                });
                return {name: e.name, values: values};
            });

            // Select SVG element, if it exists
            var svg = d3.select(this).selectAll('svg').data([data]);

            // Otherwise create the chart skeleton
            var gEnter = svg.enter().append('svg').append('g').attr('class', 'inner');
            gEnter.append('g').attr('class', 'x axis');
            gEnter.append('g').attr('class', 'y axis');
            gEnter.append('g').attr('class', 'legend')
                .attr('transform', 'translate(' + (width - margin.left - margin.right) + ', ' + margin.top + ')');

            // Update outer dimensions
            // TODO: transition?
            svg.attr('width', width)
               .attr('height', height);

            // Update inner dimensions
            // TODO: transition?
            var g = svg.select('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

            var lines = d3.select('g.inner').selectAll('g.event').data(data, function(d){ return d.name; });
            var legend = d3.select('g.legend').selectAll('g.label').data(data, function(d){ return d.name; });

            var linesEnter = lines.enter().append('g')
                .attr('class', function(d){ return 'event ' + d.name; });

            linesEnter.append('path')
                .attr('class', 'line')
                .attr('id', function(d){ return d.name; })
                .style('stroke', function(d){ return color(d.name); });

            var paths = lines.select('path.line')
                .attr('d', function(d){ return line(d.values); });

            // Find largest path in view, in order to prevent some artefact dashes at
            // the end of the line
            pathLengths = paths[0].map(function(path){
                return {
                    // find the appropriate pathlength per line, so the speed of
                    // the animation for each path is equal
                    pathName: path.id,
                    pathLength: path.getTotalLength()};
                });

            paths
                .attr('stroke-dasharray', function(d){
                    var pathLength =  _.findWhere(pathLengths, {pathName: d.name}).pathLength;
                    return pathLength + ' ' + pathLength;
                })
                .attr('stroke-dashoffset', function(d){
                    return _.findWhere(pathLengths, {pathName: d.name}).pathLength;
                })
              .transition()
                .delay(250)
                .duration(1000)
                .ease('quad-in-out')
                .attr('stroke-dashoffset', 0);

            var legendEnter = legend.enter().append('g')
                .attr('class', 'label')
                .attr('transform', 'translate(10, 0)')
                .on('mouseover', function(e){
                    d3.select('svg g.' + e.name).transition()
                        .duration(250)
                        .style('stroke-width', '3px');
                })
                .on('mouseout', function(e){
                    d3.select('svg g.' + e.name).transition()
                        .style('stroke-width', '1.5px');
                });

            legendEnter.append('circle')
                .style('fill', function(d){ return color(d.name); })
                .attr('opacity', 0);
            legendEnter.append('text')
                .attr('opacity', 0);

            legend.select('g.label circle')
                .transition()
                .attr('cx', 5)
                .attr('opacity', 1)
                .attr('cy', function(d){ return legendDistance * color.domain().indexOf(d.name); })
                .attr('r', 5);

            legend.select('g.label text')
                .transition()
                .text(function(d){
                    var sum = _.reduce(d.values, function(mem, i){ return mem + i.count; }, 0),
                        sumLength = sum.toString().length,
                        sliceOffset = (sumLength - 2 > 0) ? sumLength - 2 : 0,
                        labelCountLength = sumLength + 3; // include space and parentheses in calculation
                    return (d.name.length + labelCountLength) > 20 ? d.name.slice(0, (12 - sliceOffset)) + '... (' + sum + ')' : d.name + ' (' + sum + ')';
                })
                .attr('opacity', 1)
                .attr('x', 15)
                .attr('dy', 2.5)
                .attr('y', function(d){ return legendDistance * color.domain().indexOf(d.name); });

            g.select('.x.axis')
                .attr('transform', 'translate(0,' + yScale.range()[0] + ')')
                .transition()
                .call(xAxis);

            g.select('.y.axis')
                .transition()
                .call(yAxis);

            // For now, we will not have a very dynamic graph (since everything is
            // redrawn), but this might be useful for future updates
            lines.exit()
                .remove();

            legend.exit()
                .attr('opacity', 0)
                .remove();
        });
    }

    // The x-accessor for the path generator; xScale(xValue)
    function X(d) {
        return xScale(d.time);
    }

    // The x-accessor for the path generator; yScale(yValue)
    function Y(d) {
        return yScale(d.count);
    }

    chart.margin = function(val) {
        if (!arguments.length) return margin;
        margin = val;
        return chart;
    };

    chart.width = function(val) {
        if (!arguments.length) return width;
        width = val;
        return chart;
    };

    chart.height = function(val) {
        if (!arguments.length) return height;
        height = val;
        return chart;
    };

    chart.x = function(val){
        if(!arguments.length) return xValue;
        xValue = val;
        return chart;
    };

    chart.y = function(val){
        if(!arguments.length) return yValue;
        yValue = val;
        return chart;
    };

    chart.color = function(val){
        if(!arguments.length) return color;
        color = val;
        return chart;
    };

    chart.interval = function(val){
        if(!arguments.length) return interval;
        interval = val;
        return chart;
    };

    chart.intervalStep = function(val){
        if(!arguments.length) return intervalStep;
        intervalStep = val;
        return chart;
    };

    chart.legendDistance = function(val){
        if(!arguments.length) return legendDistance;
        legendDistance = val;
        return chart;
    };

    return chart;
}

function updateCombinedGraph(data){
    if(data.response){
        $('.empty-message').hide();
        $('.combined-graph-controls').show();
        d3.select('.combined-graph').datum(data.response).call(combinedChart);
    } else {
        $('.empty-message').show();
    }
}
