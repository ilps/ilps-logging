from functools import wraps
from datetime import datetime
from dateutil.relativedelta import relativedelta, MO
import urllib

from flask import (Blueprint, render_template, request, flash, redirect,
                   url_for, current_app, jsonify)
from flask.ext.login import login_required, login_user, logout_user, current_user
from flask.ext.mail import Message

from . import mail
from ..core import IlpsLoggingError
from ..users import verify_user
from ..services import projects, users, elasticsearch

bp = Blueprint('frontend', __name__)

def format_datetime(s):
    return s.strftime('%a %d %B %Y %H:%M:%S')

def project_sparks(projects):
    return [{'id': project.id, 'slug': project.name_slug} for project in projects]

bp.add_app_template_filter(format_datetime, 'format_datetime')
bp.add_app_template_filter(project_sparks, 'projects')

def get_project_details(fn):
    """Use the combination of project_slug and current user to fetch the
    project object and user membership and add those to the request
    object. If the project does not exist or the user is not a member,
    redirect to the project overview page. Also add the user's Kibana url
    for the project to the request object."""

    @wraps(fn)
    def wrapped_function(*args, **kwargs):
        project_slug = kwargs['project_slug']
        project = projects.get_project_by_name_slug(project_slug, current_user.id)

        if not project:
            flash(u'The project you tried to access does not exist, or you '
                  'don\'t have permission to view it.', 'alert-danger')
            return redirect(url_for('.projects_index'))

        member = projects.get_project_member(current_user.id, project.id)

        request.project = project
        request.project_member = member
        request.kibana_url = add_kibana_url()
        return fn(*args, **kwargs)

    def add_kibana_url():
        # Calculate the default timespan to request in Kibana
        now = datetime.utcnow()
        since = now - relativedelta(weeks=1)

        now = urllib.quote(str(now))
        since = urllib.quote(str(since))

        title = urllib.quote_plus(request.project.name)
        key = request.project_member.kibana_key

        kibana_url = '%s?title=%s&key=%s&from=%s&to=%s'\
            % (current_app.config['KIBANA_URL'], title, key, since, now)

        return kibana_url

    return wrapped_function


def send_mail(recipients, subject, text):
    if type(recipients) is not list:
        recipients = [recipients]

    msg = Message(subject, sender=current_app.config['DEFAULT_MAIL_SENDER'],
                  recipients=recipients, body=text)
    mail.send(msg)


@bp.route('/')
@login_required
def index():
    return render_template('index.html')


@bp.route('/login', methods=['GET', 'POST'])
def user_login():
    # Redirect the user to the index if he is already authenticated
    if current_user.is_authenticated():
        return redirect(url_for('.index'))

    if request.method == 'POST':
        try:
            user = verify_user(request.form['username'],
                               request.form['password'])
            if user:
                login_user(user)
                return redirect(request.args.get('next') or url_for('.index'))
            else:
                flash(u'We couldn\'t log you in. Please, check the email and '
                      'password you\'ve entered.', 'alert-danger')
        except IlpsLoggingError, error:
            flash(error.msg,  'alert-danger')

    return render_template('login.html')


@bp.route('/logout')
@login_required
def user_logout():
    logout_user()
    flash(u'You are now logged out.', 'alert-success')
    return redirect(url_for('.user_login'))


@bp.route('/account/register', methods=['GET', 'POST'])
def user_register():
    if request.method == 'POST':
        try:
            user = users.create(email=request.form['username'],
                                name=request.form['name'],
                                password=request.form['password'])
        except IlpsLoggingError, error:
            flash(error.msg, 'alert-danger')
            return render_template('register.html')

        # Send the welcome/verification mail
        token = user.email_verification_token
        verification_url = '%s%s' % (request.url_root[:-1],
                                     url_for('.user_verify_email',
                                             verification_token=token))
        send_mail(request.form['username'],
                  current_app.config['MESSAGES']['email_verification_subject'],
                  current_app.config['MESSAGES']['email_verification_text']
                  % (request.form['name'], verification_url))

        flash(u'Thanks for creating an account. Please check your e-mail to '
              'complete your registration.', 'alert-success')
        return redirect(url_for('.user_login'))

    return render_template('register.html')


@bp.route('/account/verify_email/<verification_token>')
def user_verify_email(verification_token):
    try:
        user = users.verify_email(verification_token)
    except IlpsLoggingError, error:
        flash(error.msg, 'alert-danger')
        return redirect(url_for('.user_login'))

    flash(u'Thanks for verifying your email address. The ILPS Logging '
          'administrators are notified about your registration. You will '
          'recieve an email as soon as your account is approved.',
          'alert-success')

    # Get all email addresses of users with an 'admin' status
    admin_emails = [u.email for u in users.find(admin=True)]

    # Notify admins of new registration
    token = user.account_approved_token

    verification_url = '%s%s' % (request.url_root[:-1],
                                 url_for('.user_admin_approve_user',
                                 verification_token=token))
    send_mail(admin_emails,
              current_app.config['MESSAGES']['request_approval_subject'],
              current_app.config['MESSAGES']['request_approval_text']
              % (user.name, user.email, verification_url))

    return redirect(url_for('.user_login'))


@bp.route('/account/approve_user/<verification_token>')
def user_admin_approve_user(verification_token):
    try:
        user = users.approve_account(verification_token)
    except IlpsLoggingError, error:
        flash(error.msg, 'alert-danger')
        return redirect(url_for('.user_login'))

    # Notify the user about the approval
    login_url = '%s%s' % (request.url_root[:-1],
                          url_for('.user_login'))
    send_mail(user.email,
              current_app.config['MESSAGES']['user_approved_subject'],
              current_app.config['MESSAGES']['user_approved_text']
              % (user.name, login_url))

    flash('Account approved!', 'alert-success')
    return redirect(url_for('.user_login'))


@bp.route('/account/change_password')
def user_change_password():
    return ''


@bp.route('/projects/')
@login_required
def projects_index():
    # Get all projects the user is a member of
    project_memberships = projects.get_projects_by_member(current_user.id)

    return render_template('projects/index.html',
                           project_memberships=project_memberships)


@bp.route('/projects/create', methods=['GET', 'POST'])
@login_required
def create_project():
    if request.method == 'POST':
        try:
            log_remote_addr = bool(request.form.get('log_remote_addr', False))
            log_user_agent = bool(request.form.get('log_user_agent', False))

            created_project = projects.create(name=request.form['name'],
                                              creator_user_id=current_user.id,
                                              description=request.form['description'],
                                              log_remote_addr=log_remote_addr,
                                              log_user_agent=log_user_agent)
        except IlpsLoggingError, error:
            flash(error.msg, 'alert-danger')
            return render_template('projects/create.html')

        # Redirect to the newly created project
        return redirect(url_for('.project_details',
                                project_slug=created_project.name_slug))

    return render_template('projects/create.html')


@bp.route('/projects/<project_slug>/')
@login_required
@get_project_details
def project_details(project_slug):
    return render_template('projects/details.html', project=request.project,
                           member=request.project_member)


@bp.route('/projects/<project_slug>/edit', methods=['GET', 'POST'])
@login_required
@get_project_details
def edit_project_details(project_slug):
    if request.method == 'POST':
        try:
            form = dict(request.form.items())
            form['log_remote_addr'] = bool(form.get('log_remote_addr', False))
            form['log_user_agent'] = bool(form.get('log_user_agent', False))
            projects.update(request.project, **form)
        except IlpsLoggingError, error:
            flash(error.msg, 'alert-danger')
        return redirect(url_for('.project_details', project_slug=project_slug))

    return render_template('projects/edit_project.html',
                           project=request.project)


@bp.route('/projects/<project_slug>/delete', methods=['GET', 'POST'])
@login_required
@get_project_details
def delete_project(project_slug):
    if request.method == 'POST':
        try:
            projects.delete(request.project)
        except IlpsLoggingError:
            raise
        return redirect(url_for('.projects_index'))

    return render_template('projects/delete_project.html',
                           project=request.project)


@bp.route('/projects/<project_slug>/members/')
@login_required
@get_project_details
def project_members(project_slug):
    return render_template('projects/members.html', project=request.project)


@bp.route('/projects/<project_slug>/members/add', methods=['GET', 'POST'])
@login_required
@get_project_details
def add_project_member(project_slug):
    if request.method == 'POST':
        # Add a new member
        user_id = request.form.get('user_id', None)
        if not user_id:
            flash(u'There was no member ID provided')
            return redirect(url_for('.add_project_member',
                                    project_slug=request.project.name_slug))

        member = projects.add_project_member(user_id, request.project.id)

        if not member:
            flash(u'Something went wrong; the member was not added to the project')
            return redirect(url_for('.add_project_member',
                                    project_slug=request.project.name_slug))

        flash(u'%s is now a project member' % member.user.name, 'alert-success')
        return redirect(url_for('.project_members',
                                project_slug=request.project.name_slug))

    current_member_ids = []
    for member in request.project.members:
        current_member_ids.append(member.user.id)

    # Get candidate project members
    curr_member_filter = users.__model__.id.in_(current_member_ids)
    membership_candidates = users.__model__.query.filter(~curr_member_filter).all()

    if not membership_candidates:
        flash(u'There are no members that can be added to this project')
        return redirect(url_for('.project_members',
                                project_slug=request.project.name_slug))

    return render_template('projects/members_add.html', project=request.project,
                           membership_candidates=membership_candidates)


@bp.route('/projects/<project_slug>/members/<int:user_id>/delete', methods=['GET', 'POST'])
@login_required
@get_project_details
def project_team_delete_member(project_slug, user_id):
    project_member = projects.get_project_member(user_id, request.project.id)

    if not project_member:
        flash(u'The user you are trying to remove from the project is not a '
               'member of this project.')
        return redirect(url_for('.project_members', project_slug=project_slug))

    if current_user.id == user_id:
        flash(u'You can\'t remove yourself from this project!')
        return redirect(url_for('.project_members', project_slug=project_slug))

    if request.method == 'POST':
        # Remove member from project
        projects.delete_project_member(project_member)
        return redirect(url_for('.project_members', project_slug=project_slug))

    return render_template('projects/delete_member.html', member=project_member)

@bp.route('/projects/<project_slug>/keys/')
@login_required
@get_project_details
def project_access_keys(project_slug):
    return render_template('projects/keys.html', project=request.project)


@bp.route('/projects/<project_slug>/keys/<access_key>/edit', methods=['GET', 'POST'])
@login_required
@get_project_details
def edit_access_key(project_slug, access_key):
    access_key = projects.get_access_key(access_key)
    if not access_key:
        flash(u'The access key you provided does not exist, or you don\'t have'
               ' permission to view it.', 'alert-danger')
        return redirect(url_for('.projects_index', project_slug=project_slug))

    if request.method == 'POST':
        # There is probably a prettier way for doing this
        kwargs = dict(request.form)
        if kwargs.get('options-query-type', None):
            if kwargs['options-query-type'][0] == 'log':
                kwargs['allows_logging'] = True
                kwargs['allows_querying'] = False
            else:
                kwargs['allows_logging'] = False
                kwargs['allows_querying'] = True
        if kwargs.get('description', None):
            kwargs['description'] = ' '.join(kwargs['description'])

        projects.update_access_key(access_key, **kwargs)
        return redirect(url_for('.project_access_keys',
                                project_slug=project_slug))

    return render_template('projects/edit_project_key.html',
                           project=request.project, access_key=access_key)


@bp.route('/projects/<project_slug>/keys/<access_key>/revoke')
@login_required
@get_project_details
def revoke_access_key(project_slug, access_key):
    try:
        projects.delete_access_key(access_key, current_user.id)
    except IlpsLoggingError, error:
        flash(error.msg, 'alert-danger')
        return redirect(url_for('.project_access_keys', project_slug=project_slug))

    flash(u'Key %s is now revoked.' % access_key, 'alert-success')
    return redirect(url_for('.project_access_keys', project_slug=project_slug))


@bp.route('/projects/<project_slug>/keys/create', methods=['GET', 'POST'])
@login_required
@get_project_details
def create_access_key(project_slug):
    if request.method == 'POST':
        allows_logging = False
        allows_querying = False
        description = None

        key_permissions = request.form.getlist('key_permissions')
        print key_permissions
        if 'allows_logging' in key_permissions:
            allows_logging = True

        if 'allows_querying' in key_permissions:
            allows_querying = True

        if 'description' in request.form and request.form['description']:
            description = request.form['description']

        try:
            projects.create_access_key(request.project.id, current_user.id,
                                       allows_logging=allows_logging,
                                       allows_querying=allows_querying,
                                       description=description)
        except IlpsLoggingError, error:
            flash(error.msg, 'alert-danger')
            return render_template('projects/create_access_key.html',
                                   project=request.project)

        flash(u'Successfully generated a new access key.', 'alert-success')
        return redirect(url_for('.project_access_keys',
                                project_slug=project_slug))

    return render_template('projects/create_access_key.html',
                           project=request.project)


@bp.route('/projects/<project_slug>/sparkline')
@login_required
def sparkline(project_slug):
    project = projects.get_project_by_name_slug(project_slug, current_user.id)
    if not project:
        return jsonify({
            'response': None,
            'error': 'The project you tried to access does not exist, or you '
                     'don\'t have permission to view it'
        })

    since = (datetime.utcnow() - relativedelta(weeks=1)).isoformat()
    query = {
        'query': {'match_all': {}},
        'facets': {
            'histogram': {
                'date_histogram': {
                    'field': 'received_at',
                    'interval': 'hour'
                },
                'facet_filter': {
                    'range': {
                        'received_at': {
                            'gte': since
                        }
                    }
                }
            }
        }
    }

    try:
        result = elasticsearch.search(query, index=project_slug,
                                      doc_type='event', size=0)
    except IlpsLoggingError:
        # No events yet
        return jsonify({
            'response': {
                'entries': [],
                'since': since
            }
        })

    return jsonify({
        'response': {
            'entries': result['facets']['histogram']['entries'],
            'since': since
        }
    })


@bp.route('/projects/<project_slug>/graph')
@login_required
def get_combinedgraph_data(project_slug):
    """
    Checks whether the currently logged in user is actually a member of this
    project, and if so, returns timeseries for the last hour per event_type in
    the index.

    TODO: make timerange configurable, so we can haz buttons that say 'last
    hour', 'last 8 hours', 'last week', etc
    """
    project = projects.get_project_by_name_slug(project_slug)
    is_member = projects.is_member(current_user.id, project.id)

    if not is_member:
        return jsonify({'error': 'This user is not a member of this project'}), 401

    graph_range = request.args.get('range', '1h')
    # Maybe we should move this to a config once
    range_intervals = {
        '1h': {'interval': 'minute', 'since': relativedelta(hours=1)},
        '8h': {'interval': '5m', 'since': relativedelta(hours=8)},
        '24h': {'interval': '15m', 'since': relativedelta(hours=24)},
        'week': {'interval': 'hour', 'since': relativedelta(days=7)},
        'month': {'interval': 'day', 'since': relativedelta(months=1)},
        'year': {'interval': 'week', 'since': relativedelta(years=1, weekday=MO(-1))}
    }

    events_query = {
        'query': {'match_all': {}},
        'facets': {
            'event_types': {
                'terms': {
                    'field': 'event_type',
                    'all_terms': True
                }
            }
        }
    }
    events = elasticsearch.search(events_query, index=project_slug,
                                  doc_type='event', size=0)
    events = [{
        'term': event['term'],
        'count': event['count']
    } for event in events['facets']['event_types']['terms']]

    since = (datetime.utcnow() - range_intervals[graph_range]['since']).isoformat()

    # query template
    query = {
        'query': {'match_all': {}},
        'facets': {}
    }

    for event in events:
        f = facet = {
            'date_histogram': {
                'field': 'received_at',
                'interval': range_intervals[graph_range]['interval']
            },
            'facet_filter': {'bool': {}}
        }
        f['facet_filter']['bool']['must'] = [{'term': {'event_type':\
            event['term']}}, {'range': {'received_at': {'gte': since}}}]

        query['facets'][event['term']] = f

    result = elasticsearch.search(query, index=project_slug, doc_type='event',
                                  size=0)

    if 'facets' not in result:
        return jsonify({'response': None})

    resp = {'facets': {}}
    for facet in result['facets']:
        if result['facets'][facet]['entries']:
            resp['facets'][facet] = {'entries': result['facets'][facet]['entries']}

    return jsonify({'response': {
        'histograms': resp['facets'],
        'since': since,
        'interval': range_intervals[graph_range]['interval']
    }})
