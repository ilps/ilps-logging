from flask.ext.login import LoginManager
from flask.ext.mail import Mail

from .. import factory
from ..users import load_user
from . import assets

login_manager = LoginManager()
login_manager.user_loader(load_user)
login_manager.login_view = '.user_login'

mail = Mail()

def create_app(settings_override=None):
    """Returns the ILPSLogging frontend application instance."""
    app = factory.create_app(__name__, __path__, settings_override)

    # Init assets
    assets.init_app(app)

    # Init Flask-Login
    login_manager.init_app(app)

    # Init Flask-Mail
    mail.init_app(app)

    return app
