from sqlalchemy.orm import relationship

from ..core import db
from ..helpers import generate_random_key


class ProjectMember(db.Model):
    __tablename__ = 'project_members'

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'),
                           primary_key=True)

    # We use this value to authenticate the user in Kibana: we tell Kibana its
    # index is an API endpoint with this key, which we can then use to validate
    # whether the user is allowed to analyze the project. This way, when the
    # user's membership to a project is revoked (s)he is no longer to access
    # Kibana
    kibana_key = db.Column(db.String(43), unique=True, default=generate_random_key)

    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)

    user = relationship('User', backref='projects')


class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True)
    name_slug = db.Column(db.String(150), unique=True)
    description = db.Column(db.Text)
    log_remote_addr = db.Column(db.Boolean, default=True, nullable=False)
    log_user_agent = db.Column(db.Boolean, default=True, nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)
    updated_at = db.Column(db.DateTime, default=db.func.now(),
                           onupdate=db.func.now(), nullable=False)

    # Cascade one-to-many, as membership and access_keys are exclusive to Project
    members = relationship('ProjectMember', backref='project', cascade='all, '
                           'delete-orphan')
    access_keys = relationship('ProjectAccessKey', backref='project',
                               lazy='joined', cascade='all, delete-orphan')

    def __repr__(self):
        return u'<Project: %s (%s)>' % (self.name, self.id)


class ProjectAccessKey(db.Model):
    __tablename__ = 'project_access_keys'

    key = db.Column(db.String(43), primary_key=True,
                    default=generate_random_key)
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    description = db.Column(db.Text)
    allows_logging = db.Column(db.Boolean, default=False, nullable=False)
    allows_querying = db.Column(db.Boolean, default=False, nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.now(), nullable=False)
    updated_at = db.Column(db.DateTime, default=db.func.now(),
                           onupdate=db.func.now(), nullable=False)
