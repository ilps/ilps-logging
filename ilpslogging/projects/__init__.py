from slugify import slugify

from ..core import SqlService, IlpsLoggingError, db
from sqlalchemy.exc import DataError
from ..elasticsearch import ElasticSearchService
from ..users import UsersService
from .models import Project, ProjectMember, ProjectAccessKey

RESERVED_PROJECT_SLUGS = set(['create'])


class ProjectsService(SqlService):
    __model__ = Project
    users = UsersService()
    es = ElasticSearchService()

    def get_project_by_name(self, project_name):
        """Returns a project instance with a specific name. Returns
        `None` if an instance with the specified project name does
        not exist.

        :param project_name: the (unique) name of the project
        """
        return self.__model__.query.filter_by(name=project_name).first()


    def get_project_by_name_slug(self, name_slug, user_id=None):
        """Returns a project instance with a specific slugified name.
        Returns `None` if an instance with the specified project slug
        does not exist.

        :param name_slug: the (unique) slugified name of the project
        """
        name_slug = self.__model__.name_slug.__eq__(name_slug)

        if user_id:
            member = self.__model__.members.any(user_id=user_id)
            query = self.__model__.query.filter(member, name_slug)
        else:
            query = self.__model__.query.filter(name_slug)

        return query.first()


    def get_access_key(self, access_key):
        """Returns a ProjectKey instance

        :param access_key: the (unique) project access key
        """
        access_key = ProjectAccessKey.query.get(access_key)

        return access_key


    def get_projects_by_member(self, user_id):
        """Returns all projects of which the user is a member.

        :param user_id: the id of the user
        """

        member = self.__model__.members.any(user_id=user_id)
        return self.__model__.query.filter(member).all()


    def add_project_member(self, user_id, project_id):
        """
        Adds the user as a project member to the project.

        :param user_id: the id of the user
        :param project_id: the id of the project the user is added to
        """
        project = self.__model__.query.get(project_id)
        if not project:
            raise IlpsLoggingError('Project does not exist')
        user = self.users.get(user_id)
        if not user:
            raise IlpsLoggingError('User does not exist')

        member = ProjectMember(user_id=user_id, project_id=project_id)
        db.session.add(member)
        db.session.commit()

        return member


    def get_project_member(self, user_id, project_id):
        """
        Get member of project.

        :param user_id: the id of the user
        :param project_id: the project the user is a member of
        """
        return ProjectMember.query.filter_by(user_id=user_id,
                                             project_id=project_id).first()


    def delete_project_member(self, project_member):
        """
        Remove user from project.

        :param project_member:
        """
        if not isinstance(project_member, ProjectMember):
            raise ValueError('%s is not of type %s' % (project_member, ProjectMember))

        # Not in session
        db.session.delete(ProjectMember.query.filter_by(user_id=project_member.user.id).first())
        db.session.commit()


    def create(self, **kwargs):
        """Returns a new, saved instance of the project model class.

        :param **kwargs: instance parameters
        """

        # Strip trailing and leading whitespace for each string value in kwargs
        for field, value in kwargs.iteritems():
            if type(value) is str:
                kwargs[field] = value

        # Check for missing fields or fields that do not contain any value
        missing_fields = []
        required_fields = ['name', 'creator_user_id']
        for field in required_fields:
            if field not in kwargs or not kwargs[field]:
                missing_fields.append(field)

        # Raise an exeception when there are missing fields
        if missing_fields:
            raise IlpsLoggingError('Missing fields or required fields are '
                                   'empty', missing_fields)

        # Check if the creator_user_id exists
        creator_user_id = kwargs['creator_user_id']
        del kwargs['creator_user_id']
        if not self.users.get(creator_user_id):
            raise IlpsLoggingError('Project creator does not exist')

        # Check if the provided project name does not yet exist
        if self.get_project_by_name(kwargs['name']):
            raise IlpsLoggingError('There already exists a project with '
                                   'the same name')

        # Slugify the project name
        kwargs['name_slug'] = slugify(kwargs['name'])

        # Check if the slug is not a reserved project name
        if kwargs['name_slug'] in RESERVED_PROJECT_SLUGS:
            raise IlpsLoggingError('The project name is not allowed, choose a '
                                   'different name')

        # Check if the name slug does not yet exist
        if self.get_project_by_name_slug(kwargs['name_slug']):
            raise IlpsLoggingError('There already exists a project with'
                                   'the same slug')

        # Create the project
        project = self.save(self.new(**kwargs), auto_commit=False)

        # Add the user that creates this project as a member
        project.members.append(ProjectMember(user_id=creator_user_id))

        # Create a default access key with which clients can p
        project.access_keys.append(ProjectAccessKey(allows_logging=True,
                                                    description='default key'))

        try:
            self.es.create_index(kwargs['name_slug'])
        except:
            raise

        db.session.commit()

        return project


    def delete(self, model):
        """Immediately deletes the specified model instance.

        :param model: the model instance to delete
        """
        self._isinstance(model)
        db.session.delete(model)

        try:
            self.es.delete_index(model.name_slug)
        except:
            raise

        db.session.commit()


    def create_access_key(self, project_id, current_user_id, **kwargs):
        """
        Add a new access key to the project after verifying that the
        project exists and that the current user is listed as a project
        member.

        :param project_id: the ID of the project to which the new key
                           should be added
        :param current_user_id: the id of the user that tries to add the
                                new key
        :param **kwargs: ProjectAccessKey instance parameters
        """
        p_id = self.__model__.id.__eq__(project_id)
        p_member = self.__model__.members.any(user_id=current_user_id)
        project = self.__model__.query.filter(p_id, p_member).first()

        if not project:
            raise IlpsLoggingError('The project does not exist or the user is '
                                   'not listed as a project member')

        # Raise an error when both querying and logging are disabled
        no_logging = ('allows_logging' not in kwargs
                      or not kwargs['allows_logging'])
        no_querying = ('allows_querying' not in kwargs
                       or not kwargs['allows_querying'])
        if no_logging and no_querying:
            raise IlpsLoggingError('Querying or logging should at least be '
                                   'allowed')

        kwargs['project_id'] = project_id

        project.access_keys.append(ProjectAccessKey(**kwargs))

        db.session.commit()

        return project


    def update_access_key(self, project_key, **kwargs):
        """Returns an updated instance of the service's model class.

        :param model: the model to update
        :param **kwargs: update parameters
        """
        if not isinstance(project_key, ProjectAccessKey):
            raise ValueError('%s is not of type %s' % (project_key, ProjectAccessKey))
        for k, v in kwargs.items():
            setattr(project_key, k, v)

        try:
            db.session.commit()
        except DataError as e:
            raise IlpsLoggingError('The access key could not be updated due to'
                                   ' a failed database operation: %s' % e.message)

        return project_key


    def delete_access_key(self, access_key, current_user_id):
        """Remove an exising access key from a project. First we verify
        that the user that tries to remove the key actually has
        permission to do so (i.e. is a project member).

        :param access_key: the access key that should be removed
        :param current_user_id: the id of the user that tries to remove
                                this key

        :returns: int -- the ID of the project to which the access_key
                         belonged
        """
        p_member = self.__model__.members.any(user_id=current_user_id)
        p_key = self.__model__.access_keys.any(key=access_key)
        project = self.__model__.query.filter(p_member, p_key).first()

        if not project:
            raise IlpsLoggingError('Access key does not exist or the user '
                                   'does not have permission to delete this '
                                   'key')

        access_key = ProjectAccessKey.query.get(access_key)

        db.session.delete(access_key)
        db.session.commit()

        return project


    def valid_access_key(self, access_key_id, project, access_type='log'):
        """
        Check whether the access_key_id belongs to the project, and has the
        proper rights as defined in access_type.

        :param access_key_id: id of the access_key
        :param project_slug: the slug of the project that is being checked
        :param access_type: the role of the access_key
        """
        allows_logging = False
        if access_type == 'log':
            allows_logging = True
        allows_querying = not allows_logging

        p_key = [access_key for access_key in project.access_keys
                    if access_key.allows_logging == allows_logging
                    and access_key.allows_querying == allows_querying
                    and access_key.key == access_key_id]
        if p_key:
            return p_key[0]
        return False


    def is_member(self, user_id, project_id):
        """
        Check whether the given user_id is member of the provided project_id

        :param user_id: the id of the user
        :param project_id: the id of the project
        """
        member = ProjectMember.query.filter_by(user_id=user_id,
                                               project_id=project_id).first()
        if member:
            return True

        return False


    def get_member_by_kibana_key(self, kibana_key):
        """
        Returns the project associated with the provided kibana_key, otherwise
        None.

        :param kibana_key: member's kibana key
        """
        return ProjectMember.query.filter_by(kibana_key=kibana_key).first()
