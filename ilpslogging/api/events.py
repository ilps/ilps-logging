from datetime import datetime

from flask import current_app
import pytz
import dateutil.parser

from . import IlpsLoggingApiError


class BaseEvent(object):
    def __init__(self, event):
        self.event = event

        required_root_fields = {
            'created_at': unicode,
            'state': dict,
            'event_type': unicode,
            'event_properties': None
        }

        # Check if required fields are present and their values are of
        # the correct type
        for field, dtype in required_root_fields.iteritems():
            if field not in event:
                raise IlpsLoggingApiError('Missing field \'%s\'' % field, 400)

            if dtype and type(event[field]) is not dtype:
                raise IlpsLoggingApiError('Incorrect datatype for \'%s\''
                                          % field, 400)

            # Don't allow empty strings
            if not event[field] and dtype is unicode:
                raise IlpsLoggingApiError('Missing value for \'%s\'' % field,
                                          400)

        # Raise an error if the 'created_at' datetime is not valid
        if not self.validate_datetime(event['created_at']):
            raise IlpsLoggingApiError('\'created_at\' is not a valid ISO-8601 '
                                      'timestamp', 400)

        self.event['received_at'] = self.get_current_time()

    @staticmethod
    def get_current_time():
        return datetime.now(pytz.timezone(current_app.config['TIME_ZONE']))\
            .isoformat()

    @staticmethod
    def validate_datetime(datetime_string):
        # Try to parse the date
        try:
            date_test = dateutil.parser.parse(datetime_string)
        except ValueError:
            return False

        # Datetime is invalid if it does not contain tz info
        if not date_test.tzinfo:
            return False

        return True


class LoginEvent(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(LoginEvent, self).__init__(*args, **kwargs)

        # 'state' should at least contain a user_id
        if 'user_id' not in self.event['state'] or\
                not self.event['state']['user_id']:
            raise IlpsLoggingApiError('Missing \'state.user_id\'', 400)


class LogoutEvent(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(LogoutEvent, self).__init__(*args, **kwargs)

        # 'state' should at least contain a user_id
        if 'user_id' not in self.event['state'] or\
                not self.event['state']['user_id']:
            raise IlpsLoggingApiError('Missing \'state.user_id\'', 400)


class MouseMovementsEvent(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(MouseMovementsEvent, self).__init__(*args, **kwargs)

        # 'event_properties' should be a list
        if type(self.event['event_properties']) is not list:
            raise IlpsLoggingApiError('\'event_properties\' should be an '
                                      'array of mouse movements', 400)

        if not self.event['event_properties']:
            raise IlpsLoggingApiError('\'event_properties\' contains no mouse '
                                      'movement items', 400)

        # Verify mouse movement items
        el_c = 0  # keeps track of the item in the array we are validating
        for mouse_movement in self.event['event_properties']:
            # verify timestamp
            if 'timestamp' not in mouse_movement:
                raise IlpsLoggingApiError('Mssing \'event_properties.%s.'
                                          'timestamp\'' % el_c, 400)

            if not self.validate_datetime(mouse_movement['timestamp']):
                raise IlpsLoggingApiError('\'event_properties.%s.timestamp\' '
                                          'is not a valid ISO-8601 timestamp',
                                          400)

            # verify mouse position
            if 'mouse_position' not in mouse_movement or \
                    type(mouse_movement['mouse_position']) is not dict:
                raise IlpsLoggingApiError('Mssing \'event_properties.%s.'
                                          'mouse_position\'' % el_c, 400)

            if ('x' not in mouse_movement['mouse_position']) or \
               ('y' not in mouse_movement['mouse_position']):
                raise IlpsLoggingApiError('X and y should be included in '
                                          '\'event_properties.%s.'
                                          'mouse_position\'' % el_c, 400)

            type_x = type(mouse_movement['mouse_position']['x'])
            type_y = type(mouse_movement['mouse_position']['y'])
            if type_x is not int or type_y is not int:
                raise IlpsLoggingApiError('X and y in \'event_properties.%s.'
                                          'mouse_position\' should both be '
                                          'integers' % el_c, 400)

            # verify viewport dimensions
            if 'viewport_dimensions' not in mouse_movement or \
                    type(mouse_movement['mouse_position']) is not dict:
                raise IlpsLoggingApiError('Mssing \'event_properties.%s.'
                                          'viewport_dimensions\'' % el_c, 400)

            if ('width' not in mouse_movement['viewport_dimensions']) or \
               ('height' not in mouse_movement['viewport_dimensions']):
                raise IlpsLoggingApiError('Width and height should be included'
                                          'in \'event_properties.%s.'
                                          'viewport_dimensions\'' % el_c, 400)

            type_width = type(mouse_movement['viewport_dimensions']['width'])
            type_height = type(mouse_movement['viewport_dimensions']['height'])
            if type_width is not int or type_height is not int:
                raise IlpsLoggingApiError('Width and height in \'event_properties.%s.'
                                          'viewport_dimensions\' should both be '
                                          'integers' % el_c, 400)
            el_c += 1


class MouseClickEvent(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(MouseClickEvent, self).__init__(*args, **kwargs)

        # 'event_properties' should be a dict
        if type(self.event['event_properties']) is not dict:
            raise IlpsLoggingApiError('\'event_properties\' should be an '
                                      'object with click details', 400)

        # verify mouse position
        if 'mouse_position' not in self.event['event_properties'] or \
                type(self.event['event_properties']['mouse_position']) is not dict:
            raise IlpsLoggingApiError('Mssing \'event_properties.'
                                      'mouse_position\'', 400)

        if ('x' not in self.event['event_properties']['mouse_position']) or \
           ('y' not in self.event['event_properties']['mouse_position']):
            raise IlpsLoggingApiError('X and y should be included in '
                                      '\'event_properties.'
                                      'mouse_position\'', 400)

        type_x = type(self.event['event_properties']['mouse_position']['x'])
        type_y = type(self.event['event_properties']['mouse_position']['y'])
        if type_x is not int or type_y is not int:
            raise IlpsLoggingApiError('X and y in \'event_properties.'
                                      'mouse_position\' should both be '
                                      'integers', 400)

        # verify node_name
        if 'node_name' not in self.event['event_properties'] or not \
                self.event['event_properties']:
            raise IlpsLoggingApiError('Missing \'event_properties.node_name\'',
                                      400)

        if type(self.event['event_properties']['node_name']) is not unicode:
            raise IlpsLoggingApiError('\'event_properties.node_name\' should '
                                      'be a string', 400)

        # verify node_classes
        if 'node_classes' in self.event['event_properties'] and\
                type(self.event['event_properties']['node_classes']) is not list:
            raise IlpsLoggingApiError('\'event_properties.node_classes\' '
                                      'should be an array', 400)

        # verify node_id, node_src and node_href
        unicode_fields = ['node_id', 'node_src', 'node_href']
        for field in unicode_fields:
            if field in self.event['event_properties'] and\
                    type(self.event['event_properties'][field]) is not unicode:
                raise IlpsLoggingApiError('\'event_properties.%s\' should '
                                          'be a string' % field, 400)

        # verify node_data_attrs
        if 'node_data_attrs' in self.event['event_properties'] and \
                type(self.event['event_properties']['node_data_attrs'])\
                is not dict:
            raise IlpsLoggingApiError('\'event_properties.node_data_attrs\' '
                                      'should be an object', 400)


class QueryEvent(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(QueryEvent, self).__init__(*args, **kwargs)

        # verify that query is in state
        if 'query' not in self.event['state'] or not\
                self.event['state']['query']:
            raise IlpsLoggingApiError('Missing \'state.query\'', 400)

        # verify that query_string is present
        if 'query_string' not in self.event['state']['query'] or not\
                self.event['state']['query']['query_string']:
            raise IlpsLoggingApiError('Missing \'state.query.query_string\'',
                                      400)


class QueryResults(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(QueryResults, self).__init__(*args, **kwargs)

        # verify that query is in state
        if 'query' not in self.event['state'] or not\
                self.event['state']['query']:
            raise IlpsLoggingApiError('Missing \'state.query\'', 400)

        # verify that query_string is present
        if 'query_string' not in self.event['state']['query'] or not\
                self.event['state']['query']['query_string']:
            raise IlpsLoggingApiError('Missing \'state.query.query_string\'',
                                      400)

        # verify that event_properties is present and a dict
        if 'event_properties' not in self.event or\
                type(self.event['event_properties']) is not dict:
            raise IlpsLoggingApiError('Missing \'event_properties\'', 400)

        # verify results
        if 'results' not in self.event['event_properties'] or\
                type(self.event['event_properties']['results']) is not list:
            raise IlpsLoggingApiError('Missing \'event_properties.results\'',
                                      400)

        # verify that n_total_results, n_displayed_results, query_time_ms
        # are ints
        int_fields = ['n_total_results', 'n_displayed_results', 'query_time_ms']
        for field in int_fields:
            if field in self.event['event_properties'] and\
                    type(self.event['event_properties'][field]) is not int:
                raise IlpsLoggingApiError('\'event_properties.%s\' should be '
                                          'an integer', 400)


class Paginate(BaseEvent):
    def __init__(self, *args, **kwargs):
        super(Paginate, self).__init__(*args, **kwargs)

        # verify that query is in state
        if 'query' not in self.event['state'] or not\
                self.event['state']['query']:
            raise IlpsLoggingApiError('Missing \'state.query\'', 400)

        # verify that query_string is present
        if 'query_string' not in self.event['state']['query'] or not\
                self.event['state']['query']['query_string']:
            raise IlpsLoggingApiError('Missing \'state.query.query_string\'',
                                      400)

        # verify that current_page is present
        if 'current_page' not in self.event['state']['query'] or not\
                self.event['state']['query']['current_page']:
            raise IlpsLoggingApiError('Missing \'state.query.current_page\'',
                                      400)
        if type(self.event['state']['query']['current_page']) is not int:
            raise IlpsLoggingApiError('\'state.query.current_page\' should be '
                                      'an integer', 400)

        # verify that event_properties is present and a dict
        if 'event_properties' not in self.event or\
                type(self.event['event_properties']) is not dict:
            raise IlpsLoggingApiError('Missing \'event_properties\'', 400)

        if 'to_page_n' not in self.event['event_properties'] or\
                type(self.event['event_properties']['to_page_n']) is not int:
            raise IlpsLoggingApiError('\'event_properties.to_page_n\' should '
                                      'be an integer', 400)


class EventValidator(object):
    event_types = {
        'login': LoginEvent,
        'logout': LogoutEvent,
        'mouse_movements': MouseMovementsEvent,
        'mouse_click': MouseClickEvent,
        'query': QueryEvent,
        'query_results': QueryResults,
        'paginate': Paginate
    }

    def validate(self, event):
        if event['event_type'] in self.event_types:
            return self.event_types[event['event_type']](event)
        else:
            return BaseEvent(event)
