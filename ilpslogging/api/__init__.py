from functools import update_wrapper, wraps
from datetime import timedelta

from flask import request, jsonify, make_response, current_app

from .. import factory
from ..services import projects, elasticsearch


def create_app(settings_override=None):
    """Returns the ILPSLogging API application instance."""
    app = factory.create_app(__name__, __path__, settings_override)
    app.errorhandler(IlpsLoggingApiError)(on_api_error)

    app.es = elasticsearch

    return app


class IlpsLoggingApiError(Exception):
    """API error class."""

    def __init__(self, msg, status_code):
        self.msg = msg
        self.status_code = status_code

    def __str__(self):
        return repr(self.msg)


def decode_json_post_data(fn):
    @wraps(fn)
    def wrapped_function(*args, **kwargs):
        if request.method == 'POST':
            request.data = request.get_json(force=True)
            if not request.data:
                raise IlpsLoggingApiError('No query or log JSON posted', 400)

        return fn(*args, **kwargs)

    return wrapped_function


def require_valid_access_key(actions):
    def decorator(fn):
        def wrapped_function(*args, **kwargs):
            # First check if the key was passed via basicauth (in the
            # username field) or as a URL param ('api_key').
            key = None
            if request.authorization:
                key = request.authorization.get('username')
            elif request.args.get('api_key'):
                key = request.args.get('api_key')

            if not key:
                raise IlpsLoggingApiError('No access_key provided', 401)

            key = projects.get_access_key(key)
            if not key:
                raise IlpsLoggingApiError('Invalid access_key', 401)

            if 'query' in actions and not key.allows_querying:
                raise IlpsLoggingApiError('Querying with this access_key is '
                                          'not allowed', 403)

            if 'log' in actions and not key.allows_logging:
                raise IlpsLoggingApiError('Logging is not allowed with this '
                                          'access_key', 403)
            request.project = key.project

            return fn(*args, **kwargs)
        return update_wrapper(wrapped_function, fn)
    return decorator


def require_valid_kibana_key(f):
    @wraps(f)
    def wrapped_function(*args, **kwargs):
        key = kwargs.get('kibana_key', None)
        if not key:
            raise IlpsLoggingApiError('No Kibana key provided', 400)
        member = projects.get_member_by_kibana_key(key)
        if not member:
            raise IlpsLoggingApiError('This key is not allowed to view any '
                                      'projects', 403)
        request.project = member.project

        return f(*args, **kwargs)
    return wrapped_function


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Co'\
                                                'ntent-Type, Accept, Authoriz'\
                                                'ation'
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


@crossdomain(origin='*')
def on_api_error(e):
    return jsonify(dict(status='error', error=e.msg)), e.status_code
