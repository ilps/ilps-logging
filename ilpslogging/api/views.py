from flask import Blueprint, request, current_app, jsonify

from ..core import IlpsLoggingError
from . import (require_valid_access_key, decode_json_post_data, crossdomain,
               require_valid_kibana_key, IlpsLoggingApiError)
from .events import EventValidator

bp = Blueprint('api', __name__)

validator = EventValidator()


@bp.route('/log', methods=['POST'])
@require_valid_access_key(['log'])
@decode_json_post_data
@crossdomain(origin='*')
def log():
    """Validate and store a single event."""
    doc = validator.validate(request.data).event

    if request.project.log_remote_addr:
        doc['state']['ip_address'] = request.remote_addr

    if request.project.log_user_agent:
        doc['state']['user_agent_string'] = request.user_agent.string
        doc['state']['platform'] = request.user_agent.platform
        doc['state']['browser'] = request.user_agent.browser
        doc['state']['browser_verions'] = request.user_agent.version
        doc['state']['language'] = request.user_agent.language
    try:
        result = current_app.es.index(request.project.name_slug, 'event',
                                      doc)
    except:
        raise IlpsLoggingApiError('A problem with the storage backend '
                                  'occurred, the event is not saved', 500)

    return jsonify({'status': 'ok', 'id': result['_id']})


@bp.route('/bulklog', methods=['POST', 'OPTIONS'])
@require_valid_access_key(['log'])
@decode_json_post_data
@crossdomain(origin='*', methods=['POST', 'OPTIONS'], headers=['Authorization'])
def bulk_log():
    """Validate and store multiple events."""

    data = request.data

    # Validate that we acutally recieved some events to process
    if type(data) is not list:
        raise IlpsLoggingApiError('Should have recieved an array of events',
                                  400)
    if len(data) < 1:
        raise IlpsLoggingApiError('Events array is empty', 400)

    events = []
    for event in data:
        es_doc = {
            'index': request.project.name_slug,
            'type': 'event',
            'doc': validator.validate(event).event
        }

        if request.project.log_remote_addr:
            es_doc['doc']['state']['ip_address'] = request.remote_addr

        if request.project.log_user_agent:
            es_doc['doc']['state']['user_agent_string'] = request.user_agent.string
            es_doc['doc']['state']['platform'] = request.user_agent.platform
            es_doc['doc']['state']['browser'] = request.user_agent.browser
            es_doc['doc']['state']['browser_version'] = request.user_agent.version
            es_doc['doc']['state']['language'] = request.user_agent.language

        events.append(es_doc)

    try:
        current_app.es.bulk_index(events)
    except:
        raise

    return jsonify({'status': 'ok'})


@bp.route('/kibana/<kibana_key>/<es_endpoint>', methods=['GET', 'POST', 'OPTIONS'])
@require_valid_kibana_key
@crossdomain(origin='*', headers=['Origin', 'X-Requested-With', 'Content-Type', 'Accept'])
@decode_json_post_data
def proxy_kibana(kibana_key, es_endpoint):
    allowed_endpoints = ['_search', '_mapping']
    if es_endpoint not in allowed_endpoints:
        raise IlpsLoggingApiError('Not an allowed ES endpoint', 403)

    index = request.project.name_slug
    if es_endpoint == '_mapping' and request.method == 'GET':
        mapping = current_app.es.get_mapping(index=index)
        return jsonify(mapping)

    if es_endpoint == '_search' and request.method == 'POST':
        size = request.data.pop('size', 0)
        result = current_app.es.search(request.data, index=index,
                                       doc_type='event', size=size)
        return jsonify(result)

    # TODO: somehow return something here that generates a redirect
    return jsonify({'response': None})


@bp.route('/query', methods=['POST'])
@require_valid_access_key(['query'])
@decode_json_post_data
def query():
    size = request.data.pop('size', 10)
    offset = request.data.pop('offset', 0)

    try:
        result = current_app.es.search(request.data,
                                       index=request.project.name_slug,
                                       doc_type='event', size=size,
                                       offset=offset)
    except IlpsLoggingError, e:
        raise IlpsLoggingApiError(e.msg, 500)

    return jsonify(result)
