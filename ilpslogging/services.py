from .users import UsersService
from .projects import ProjectsService
from .elasticsearch import ElasticSearchService

users = UsersService()
projects = ProjectsService()
elasticsearch = ElasticSearchService()
