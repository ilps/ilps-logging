import pkgutil
import importlib
import hashlib
import random
import base64

from flask import Blueprint


def register_blueprints(app, package_name, package_path):
    """ Register all Blueprint instances on the specified Flask
    application found in all modules for the specified package.

    :param app: the Flask application
    :param package_name: the package name
    :param package_path: the package path
    """
    rv = []
    for _, name, _ in pkgutil.iter_modules(package_path):
        m = importlib.import_module('%s.%s' % (package_name, name))
        for item in dir(m):
            item = getattr(m, item)
            if isinstance(item, Blueprint):
                app.register_blueprint(item)
            rv.append(item)
    return rv


def generate_random_key():
    """ Generate a random API Key based on a 256-bit random number. The
    final key is enoced as a url safe Base64 string with a length of 43
    chars."""
    # Get 256 random bits and hash them using sha256
    random_key = hashlib.sha256(str(random.getrandbits(256))).digest()

    # Provide alternatives for '+' and '/' chars (to make sure the generated
    # base64 encoded string is URL safe)
    altchars = random.choice(['rA', 'aZ', 'gQ', 'hH', 'hG', 'aR', 'DD'])

    # Base64 encode the random key
    random_key = base64.b64encode(random_key, altchars).rstrip('==')

    return random_key
