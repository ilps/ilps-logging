#!/usr/bin/env python

from flask.ext.script import Manager
from werkzeug.serving import run_simple

from ilpslogging import frontend
from ilpslogging.manage import BuildJSSDK, users_commands, db_commands

from wsgi import application

manager = Manager(frontend.create_app())
manager.add_command('build_jssdk', BuildJSSDK())
manager.add_command('db', db_commands)
manager.add_command('users', users_commands)

@manager.command
def runserver():
    """Run the Flask development server in debug mode"""
    run_simple('0.0.0.0', 5000, application, use_reloader=True, use_debugger=True)

if __name__ == '__main__':
    manager.run()
